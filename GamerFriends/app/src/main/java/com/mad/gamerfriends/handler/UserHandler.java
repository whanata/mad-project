package com.mad.gamerfriends.handler;

import com.google.firebase.database.DataSnapshot;
import com.mad.gamerfriends.db.FirebaseDb;
import com.mad.gamerfriends.model.FriendList;
import com.mad.gamerfriends.model.Game;
import com.mad.gamerfriends.model.User;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Handles all the activity associated with a user and their friends on Steam.
 * @author Wirawan Tjo
 * @version 1.0
 */
public class UserHandler {
    public static final String LOG_TAG = "UserHandler";

    public static final String SORT_BY_NAME = "Sort By UserName";

    protected User mUser;
    protected FirebaseDb mFirebaseDb;

    /**
     * Constructor - initialise new User and FirebaseDB.
     * @param steamId Steam ID of user
     */
    public UserHandler(String steamId) {
        this.mFirebaseDb = FirebaseDb.getInstance();
        this.mUser = new User(steamId);
    }

    /**
     * Constructor - initialise User using a current User Object and Firebase DB.
     * @param user User Object
     */
    public UserHandler(User user) {
        this.mFirebaseDb = FirebaseDb.getInstance();
        this.mUser = new User(user);
    }

    /**
     * Get User from FirebaseDB using Disposable
     * @param observer Observer for this disposable
     * @return Disposable for getting the user information from Firebase DB
     */
    public Disposable getUser(DisposableObserver<DataSnapshot> observer) {
        return this.mFirebaseDb.getUser(this.mUser.getSteamId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(observer);
    }

    /**
     * Initialise a new user using the result from FirebaseDB (DataSnapshot)
     * @param dataSnapshot User Information result from Firebase DB
     */
    public void getUserResult(DataSnapshot dataSnapshot) {
        this.mUser = new User(dataSnapshot.getValue(User.class));
    }

    /**
     * Getting friend list from Firebase DB and the user information for each friend.
     * Get the friend list (list of steam ID) then for each friend, get the user information from
     * FirebaseDB.
     * @param observer Observer for this disposable
     * @return Disposable for getting the User information for each friend
     */
    public Disposable getFriendList(DisposableObserver<DataSnapshot> observer) {
        this.mUser.clearFriendList();
        return mFirebaseDb.getFriendList(this.mUser.getSteamId())
                .flatMapIterable(new Function<DataSnapshot, Iterable<String>>() {

                    @Override
                    public Iterable<String> apply(@NonNull DataSnapshot dataSnapshot)
                            throws Exception {
                        @SuppressWarnings("unchecked")
                        Map<String, String> result = (Map<String, String>) dataSnapshot.getValue();
                        return new ArrayList<>(result.keySet());
                    }
                })
                .flatMap(new Function<String, ObservableSource<DataSnapshot>>() {

                    @Override
                    public ObservableSource<DataSnapshot> apply(@NonNull String steamId)
                            throws Exception {
                        return mFirebaseDb.getUser(steamId)
                                .subscribeOn(Schedulers.io());
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(observer);
    }

    /**
     * Initialise a new friend using the result from FirebaseDB (DataSnapshot)
     * @param dataSnapshot Friend Information result from Firebase DB
     */
    public void getFriendListResult(DataSnapshot dataSnapshot) {
        User user = new User(dataSnapshot.getValue(User.class));
        this.mUser.addFriend(user);
    }

    /**
     * Get steam ID
     * @return Steam ID
     */
    public String getSteamId() {
        return this.mUser.getSteamId();
    }

    /**
     * Set steam ID
     * @param steamId Steam ID
     */
    public void setSteamId(String steamId) {
        this.mUser.setSteamId(steamId);
    }

    /**
     * Get username of user
     * @return Username
     */
    public String getUsername() {
        return this.mUser.getUsername();
    }

    /**
     * Get Friend List object, from user assign to this handler.
     * @return Friend List Object
     */
    public FriendList getFriendList() {
        return this.mUser.getFriendList();
    }

    /**
     * Get Avatar URL for the user assign to this handler.
     * @return Avatar URL for a user
     */
    public String getAvatarUrl() {
        return this.mUser.getAvatarUrl();
    }

    /**
     * Get User Object from Friend List in the user assign to this handler.
     * @param steamId Steam ID of friend
     * @return User object (friend)
     */
    public User getUserFromFriendList(String steamId) {
        return this.mUser.getUserFromFriendList(steamId);
    }

    /**
     * Sort Friend List by name
     * @param by String for which list to sort.
     */
    public void sortFriendList(String by) {
        switch(by) {
            case UserHandler.SORT_BY_NAME:
                List<User> users = this.getFriendList().getUsers();
                Collections.sort(users, User.COMPARE_BY_NAME_ASC);
                break;
            default:
                break;
        }
    }

    /**
     * Add game to user object assign to this handler.
     * @param game Game Object
     */
    public void addGame(Game game) {
        this.mUser.addGame(game);
    }

    /**
     * Get game list from user object assign to this handler.
     * @return List of games
     */
    public List<Game> getGameList() {
        return this.mUser.getGames();
    }
}
