package com.mad.gamerfriends.adapter;

import android.support.annotation.LayoutRes;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.Query;
import com.koushikdutta.ion.Ion;
import com.mad.gamerfriends.R;
import com.mad.gamerfriends.handler.ChatHandler;
import com.mad.gamerfriends.handler.CurrentUserHandler;
import com.mad.gamerfriends.model.ChatMessage;
import com.makeramen.roundedimageview.RoundedImageView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * A RecyclerAdapter extending FirebaseRecyclerAdapter for FirebaseDB. This class is to control the
 * RecyclerView for chat, specifically using Firebase UI DB since it updates from FirebaseDB.
 * @author  Wirawan Tjo
 * @version 1.0
 */
public class ChatRecyclerAdapter extends FirebaseRecyclerAdapter<ChatMessage, ChatRecyclerAdapter.ViewHolder> {
    private ChatHandler mChatHandler;
    private LinearLayoutManager mLinearLayoutManager;

    /**
     * Instantiates ChatRecyclerAdapter.
     * @param chatHandler The ChatHandler that was used in the ChatFragment.
     * @param linearLayoutManager The LinearLayoutManager that was used for the RecyclerView.
     * @param modelLayout The ID for the layout for each item in the adapter.
     * @param query The query used to get the items to fill this RecyclerView.
     */
    public ChatRecyclerAdapter(ChatHandler chatHandler, LinearLayoutManager linearLayoutManager,
                               @LayoutRes int modelLayout, Query query) {
        super(ChatMessage.class, modelLayout, ChatRecyclerAdapter.ViewHolder.class, query);
        this.mChatHandler = chatHandler;
        this.mLinearLayoutManager = linearLayoutManager;
    }

    /**
     * Inner class - Represents each item in the RecyclerView. Each instance of this class is one
     * item in the RecyclerView.
     */
    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_chat_receive_message_TextView)
        protected TextView mChatReceiveMessageTextView;
        @BindView(R.id.item_chat_receive_profile_image_RoundedImageView)
        protected RoundedImageView mChatReceiveProfileRoundedImageView;
        @BindView(R.id.item_chat_receive_ConstraintLayout)
        ConstraintLayout mChatReceiveConstraintLayout;

        @BindView(R.id.item_chat_send_message_TextView)
        protected TextView mChatSendMessageTextView;
        @BindView(R.id.item_chat_send_profile_image_RoundedImageView)
        protected RoundedImageView mChatSendProfileRoundedImageView;
        @BindView(R.id.item_chat_send_ConstraintLayout)
        ConstraintLayout mChatSendConstraintLayout;

        /**
         * Instantiates the View Element in each item from the item layout.
         * @param itemView
         */
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        /**
         * Set the message for each item. Also checks if message is from the current user or
         * messages that are getting received, which determine the layout of the message item.
         * @param chatHandler
         * @param chatMessage
         */
        public void setMessage(ChatHandler chatHandler,
                               ChatMessage chatMessage) {
            CurrentUserHandler currentUser = CurrentUserHandler.getInstance();
            String currentUserAvatarUrl = currentUser.getAvatarUrl();
            String receiverUserAvatarUrl = chatHandler.getReceiverAvatarUrl();
            String messageSteamId = chatMessage.getSteamId();

            // Current User Message (Sending message)
            if (messageSteamId.equals(currentUser.getSteamId())) {
                this.mChatReceiveConstraintLayout.setVisibility(View.GONE);
                this.mChatSendMessageTextView.setText(chatMessage.getText());
                this.mChatSendConstraintLayout.setVisibility(View.VISIBLE);
                Ion.with(this.mChatSendProfileRoundedImageView)
                        .placeholder(R.drawable.ic_placeholder_profile)
                        .error(R.drawable.ic_placeholder_profile)
                        .animateIn(R.anim.fade_in)
                        .load(currentUserAvatarUrl);
            } else {
                // Receiving messages
                this.mChatSendConstraintLayout.setVisibility(View.GONE);
                this.mChatReceiveMessageTextView.setText(chatMessage.getText());
                this.mChatReceiveConstraintLayout.setVisibility(View.VISIBLE);
                Ion.with(this.mChatReceiveProfileRoundedImageView)
                        .placeholder(R.drawable.ic_placeholder_profile)
                        .error(R.drawable.ic_placeholder_profile)
                        .animateIn(R.anim.fade_in)
                        .load(receiverUserAvatarUrl);
            }

        }
    }

    /**
     * Set the message for each message item.
     * @param viewHolder The ViewHolder instance
     * @param chatMessage The message object from Firebase DB
     * @param position Position on the RecyclerView
     */
    @Override
    protected void populateViewHolder(ViewHolder viewHolder, ChatMessage chatMessage, int position) {
        viewHolder.setMessage(this.mChatHandler, chatMessage);
    }

    /**
     * Whenever data changes (new data is inserted, scroll to the bottom)
     * @param type Event Type
     * @param snapshot DataSnapShot
     * @param index Index on Firebase DB
     * @param oldIndex The change in index
     */
    @Override
    public void onChildChanged(EventType type, DataSnapshot snapshot, int index, int oldIndex) {
        super.onChildChanged(type, snapshot, index, oldIndex);
        this.mLinearLayoutManager.scrollToPosition(getItemCount() - 1);
    }

    /**
     * Since mLinearLayoutManager uses context, making this null will ensure that the activity
     * will not leak if we use this class again.
     */
    @Override
    public void cleanup() {
        super.cleanup();
        this.mLinearLayoutManager = null;
    }
}
