package com.mad.gamerfriends.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mad.gamerfriends.R;
import com.mad.gamerfriends.activity.LoginActivity;
import com.mad.gamerfriends.handler.CurrentUserHandler;
import com.mad.gamerfriends.helper.PreferenceHelper;
import com.mad.gamerfriends.login.SteamLogin;

import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * Fragment for the settings of the app, basically to log out of the app. This is in the
 * MainActivity.
 * @author Wirawan Tjo
 * @version 1.0
 */
public class SettingFragment extends Fragment {
    Unbinder mUnbinder;
    CurrentUserHandler mCurrentUserHander;

    /**
     * Create new instance of this fragment.
     * @return SettingFragment instance
     */
    public static SettingFragment newInstance() {
        return new SettingFragment();
    }

    /**
     * Initialise variables that doesn't require the view.
     * @param savedInstanceState Saved information from previous state
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mCurrentUserHander = CurrentUserHandler.getInstance();
    }

    /**
     * Instantiate variables that require a view. This includes butterknife instance.
     * @param inflater Used to inflate the fragment's view on the activity
     * @param container The container where the fragment's view is inflated.
     * @param savedInstanceState Saved information from previous state
     * @return The view for this fragment.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_setting, container, false);
        this.mUnbinder = ButterKnife.bind(this, view);

        return view;
    }

    /**
     * Unbind the Butterknife Instance.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mUnbinder.unbind();
    }

    /**
     * On clicking logout button, remove the Steam ID in shared preference, remove the instance
     * in the CurrentUserHandler and redirect to LoginActivity.
     */
    @OnClick(R.id.fragment_setting_logout_Button)
    public void logoutAction() {
        this.mCurrentUserHander.logOut();
        PreferenceHelper preference = new PreferenceHelper(getActivity());
        preference.removeString(SteamLogin.STEAM_ID_KEY);
        Intent logoutIntent = new Intent(getActivity(), LoginActivity.class);
        startActivity(logoutIntent);
        getActivity().finish();
    }

}