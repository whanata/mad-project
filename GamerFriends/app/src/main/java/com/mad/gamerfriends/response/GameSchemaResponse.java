package com.mad.gamerfriends.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by whanata on 24/9/17.
 */

public class GameSchemaResponse {
    @SerializedName("game")
    private Game game;

    public GameSchemaResponse() {
        this.game = new Game();
    }

    public Game getGame() {
        return this.game;
    }

    public String getGameName() {
        return this.game.getGameName();
    }

    public String getGameVersion() {
        return this.game.getGameVersion();
    }

    public Game.GameAvailableStatsResponse getPossibleStats() {
        return this.game.getPossibleStats();
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public class Game {
        @SerializedName("gameName")
        private String mGameName;
        @SerializedName("gameVersion")
        private String mGameVersion;
        @SerializedName("availableGameStats")
        private GameAvailableStatsResponse mPossibleStats;

        public Game() {
            this.mPossibleStats = new GameAvailableStatsResponse();
        }

        public Game(
                String mGameName,
                String mGameVersion,
                GameAvailableStatsResponse possibleStats) {
            this.mGameName = mGameName;
            this.mGameVersion = mGameVersion;
            this.mPossibleStats = possibleStats;
        }

        public String getGameName() {
            return this.mGameName;
        }

        public void setGameName(String gameName) {
            this.mGameName = gameName;
        }

        public String getGameVersion() {
            return this.mGameVersion;
        }

        public void setGameVersion(String gameVersion) {
            this.mGameVersion = gameVersion;
        }

        public GameAvailableStatsResponse getPossibleStats() {
            return this.mPossibleStats;
        }

        public void setPossibleStats(GameAvailableStatsResponse possibleStats) {
            this.mPossibleStats = possibleStats;
        }

        public class GameAvailableStatsResponse {
            @SerializedName("achievements")
            private List<GameAchievementResponse> mAchievements;
            @SerializedName("stats")
            private List<GameStatResponse> mStats;

            public GameAvailableStatsResponse() {
                this.mAchievements = new ArrayList<>();
                this.mStats = new ArrayList<>();
            }

            public GameAvailableStatsResponse(
                    List<GameAchievementResponse> mAchievements,
                    List<GameStatResponse> mStats) {
                this.mAchievements = mAchievements;
                this.mStats = mStats;
            }

            public List<GameAchievementResponse> getAchievements() {
                if (this.mAchievements == null) {
                    this.mAchievements = new ArrayList<>();
                }
                return this.mAchievements;
            }

            public void setAchievements(List<GameAchievementResponse> achievements) {
                this.mAchievements = achievements;
            }

            public List<GameStatResponse> getStats() {
                if (this.mStats == null) {
                    this.mStats = new ArrayList<>();
                }
                return this.mStats;
            }

            public void setStats(List<GameStatResponse> stats) {
                this.mStats = stats;
            }
        }
    }
}
