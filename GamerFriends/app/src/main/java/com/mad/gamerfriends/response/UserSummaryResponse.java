package com.mad.gamerfriends.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by whanata on 2/9/17.
 */

public class UserSummaryResponse {
    @SerializedName("response")
    private Users mUsers;

    public UserSummaryResponse(Users mUsers) {
        this.mUsers = mUsers;
    }

    public List<UserResponse> getUsers() {
        return this.mUsers.getUserList();
    }

    public void setUsers(Users users) {
        this.mUsers = users;
    }

    private class Users {
        @SerializedName("players")
        private List<UserResponse> mUserList;

        public Users(List<UserResponse> mUserList) {
            this.mUserList = mUserList;
        }

        public List<UserResponse> getUserList() {
            return this.mUserList;
        }

        public void setUserList(List<UserResponse> userList) {
            this.mUserList = userList;
        }
    }
}
