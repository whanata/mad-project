package com.mad.gamerfriends.api_client;

import com.mad.gamerfriends.response.FriendListResponse;
import com.mad.gamerfriends.response.GameSchemaResponse;
import com.mad.gamerfriends.response.OwnedGameListResponse;
import com.mad.gamerfriends.response.UserGameStatResponse;
import com.mad.gamerfriends.response.UserSummaryResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Interface for Steam Web API.
 * @author  Wirawan Tjo
 * @version 1.0
 */
public interface SteamWebClient {
    /**
     * Get Friend List
     * @param key API key
     * @param steamId Steam ID
     * @return FriendListResponse Observable
     */
    @GET("/ISteamUser/GetFriendList/v0001?relationship=friend")
    Observable<FriendListResponse> getFriendList(
            @Query("key") String key,
            @Query("steamid") String steamId
    );

    /**
     * Get Player Summary
     * @param key API Key
     * @param steamIds Steam IDs separated by a comma
     * @return UserSummaryResponse Observable
     */
    @GET("/ISteamUser/GetPlayerSummaries/v0002")
    Observable<UserSummaryResponse> getUserSummary(
            @Query("key") String key,
            @Query("steamids") String steamIds
    );

    /**
     * Get Owned Games
     * @param key API Key
     * @param steamId Steam ID
     * @return OwnedGameListRespone Observable
     */
    @GET("/IPlayerService/GetOwnedGames/v0001/?include_appinfo=1&include_played_free_games=1")
    Observable<OwnedGameListResponse> getOwnedGames(
            @Query("key") String key,
            @Query("steamId") String steamId
    );

    /**
     * Get Stats and Achievements for a user in a specific game
     * @param key API Key
     * @param appId App ID for game by Steam
     * @param steamId Steam ID
     * @return UserGameStatResponse Observable
     */
    @GET("/ISteamUserStats/GetUserStatsForGame/v0002")
    Observable<UserGameStatResponse> getUserGameStats(
            @Query("key") String key,
            @Query("appid") String appId,
            @Query("steamid") String steamId
    );

    /**
     * Get Game Information
     * @param key API Key
     * @param appId App ID for game by Steam
     * @return GameSchemaResponse Observable
     */
    @GET("/ISteamUserStats/GetSchemaForGame/v2")
    Observable<GameSchemaResponse> getGameSchema(
            @Query("key") String key,
            @Query("appid") String appId
    );
}
