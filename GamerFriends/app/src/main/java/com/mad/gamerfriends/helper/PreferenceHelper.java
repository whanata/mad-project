package com.mad.gamerfriends.helper;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Helps with doing shared preferences.
 * @author Wirawan Tjo
 * @version 1.0
 */
public class PreferenceHelper {
    private SharedPreferences mSharedPreferences;
    private SharedPreferences.Editor mEditor;

    /**
     * Constructor - Create Shared PreferenceManager and editor.
     * @param context
     */
    public PreferenceHelper(Context context) {
        this.mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        this.mEditor = mSharedPreferences.edit();
    }

    /**
     * Add string to Shared Preference by a key
     * @param key Key for calling the value
     * @param value String value itself
     */
    public void addString(String key, String value) {
        this.mEditor.putString(key, value);
        this.mEditor.apply();
    }

    /**
     * Get string from Shared Preference by Key
     * @param key Key for a string in Shared Preference
     * @return String value
     */
    public String getString(String key) {
        return this.mSharedPreferences.getString(key, null);
    }

    /**
     * Remove a string from Shared Preference by Key
     * @param key Key for a string in Shared Preference
     */
    public void removeString (String key) {
        this.mEditor.remove(key);
        this.mEditor.apply();
    }

    /**
     * Commit a change in Shared Preference.
     */
    public void commit() {
        this.mEditor.commit();
    }

}
