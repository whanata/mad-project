package com.mad.gamerfriends.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by whanata on 2/9/17.
 */

public class UserResponse {
    @SerializedName("steamid")
    private String mSteamId;
    @SerializedName("personaname")
    private String mUserName;
    @SerializedName("profileurl")
    private String mProfileUrl;
    @SerializedName("avatar")
    private String mAvatar;
    @SerializedName("avatarmedium")
    private String mAvatarMedium;
    @SerializedName("avatarfull")
    private String mAvatarFull;
    @SerializedName("personastate")
    private String mPersonaState;
    @SerializedName("communityvisibilitystate")
    private String mCommunityVisibilityState;
    @SerializedName("profilestate")
    private String mProfileState;
    @SerializedName("lastlogoff")
    private String mLastLogOff;
    @SerializedName("commentpermission")
    private String mCommentPermission;
    @SerializedName("realname")
    private String mRealName;
    @SerializedName("primaryclanid")
    private String mPrimaryClanId;
    @SerializedName("timecreated")
    private String mTimeCreated;
    @SerializedName("gameid")
    private String mGameId;
    @SerializedName("gameserverip")
    private String mGameServerIp;
    @SerializedName("gameextrainfo")
    private String mGameExtraInfo;
    @SerializedName("cityid")
    private String mCityId;
    @SerializedName("loccountrycode")
    private String mCountryCode;
    @SerializedName("locstatecode")
    private String mStateCode;
    @SerializedName("loccityid")
    private String mLocCityId;

    public UserResponse(String mSteamId, String mUserName, String mProfileUrl, String mAvatar, String mAvatarMedium, String mAvatarFull, String mPersonaState, String mCommunityVisibilityState, String mProfileState, String mLastLogOff, String mCommentPermission, String mRealName, String mPrimaryClanId, String mTimeCreated, String mGameId, String mGameServerIp, String mGameExtraInfo, String mCityId, String mCountryCode, String mStateCode, String mLocCityId) {
        this.mSteamId = mSteamId;
        this.mUserName = mUserName;
        this.mProfileUrl = mProfileUrl;
        this.mAvatar = mAvatar;
        this.mAvatarMedium = mAvatarMedium;
        this.mAvatarFull = mAvatarFull;
        this.mPersonaState = mPersonaState;
        this.mCommunityVisibilityState = mCommunityVisibilityState;
        this.mProfileState = mProfileState;
        this.mLastLogOff = mLastLogOff;
        this.mCommentPermission = mCommentPermission;
        this.mRealName = mRealName;
        this.mPrimaryClanId = mPrimaryClanId;
        this.mTimeCreated = mTimeCreated;
        this.mGameId = mGameId;
        this.mGameServerIp = mGameServerIp;
        this.mGameExtraInfo = mGameExtraInfo;
        this.mCityId = mCityId;
        this.mCountryCode = mCountryCode;
        this.mStateCode = mStateCode;
        this.mLocCityId = mLocCityId;
    }

    public String getSteamId() {
        return this.mSteamId;
    }

    public void setSteamId(String steamId) {
        this.mSteamId = steamId;
    }

    public String getUserName() {
        return this.mUserName;
    }

    public void setUserName(String userName) {
        this.mUserName = userName;
    }

    public String getProfileUrl() {
        return this.mProfileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.mProfileUrl = profileUrl;
    }

    public String getAvatar() {
        return this.mAvatar;
    }

    public void setAvatar(String avatar) {
        this.mAvatar = avatar;
    }

    public String getAvatarMedium() {
        return this.mAvatarMedium;
    }

    public void setAvatarMedium(String avatarMedium) {
        this.mAvatarMedium = avatarMedium;
    }

    public String getAvatarFull() {
        return this.mAvatarFull;
    }

    public void setAvatarFull(String avatarFull) {
        this.mAvatarFull = avatarFull;
    }

    public String getPersonaState() {
        return this.mPersonaState;
    }

    public void setPersonaState(String personaState) {
        this.mPersonaState = personaState;
    }

    public String getCommunityVisibilityState() {
        return this.mCommunityVisibilityState;
    }

    public void setCommunityVisibilityState(String communityVisibilityState) {
        this.mCommunityVisibilityState = communityVisibilityState;
    }

    public String getProfileState() {
        return this.mProfileState;
    }

    public void setProfileState(String profileState) {
        this.mProfileState = profileState;
    }

    public String getLastLogOff() {
        return this.mLastLogOff;
    }

    public void setLastLogOff(String lastLogOff) {
        this.mLastLogOff = lastLogOff;
    }

    public String getCommentPermission() {
        return this.mCommentPermission;
    }

    public void setCommentPermission(String commentPermission) {
        this.mCommentPermission = commentPermission;
    }

    public String getRealName() {
        return this.mRealName;
    }

    public void setRealName(String realName) {
        this.mRealName = realName;
    }

    public String getPrimaryClanId() {
        return this.mPrimaryClanId;
    }

    public void setPrimaryClanId(String primaryClanId) {
        this.mPrimaryClanId = primaryClanId;
    }

    public String getTimeCreated() {
        return this.mTimeCreated;
    }

    public void setTimeCreated(String timeCreated) {
        this.mTimeCreated = timeCreated;
    }

    public String getGameId() {
        return this.mGameId;
    }

    public void setGameId(String gameId) {
        this.mGameId = gameId;
    }

    public String getGameServerIp() {
        return this.mGameServerIp;
    }

    public void setGameServerIp(String gameServerIp) {
        this.mGameServerIp = gameServerIp;
    }

    public String getGameExtraInfo() {
        return this.mGameExtraInfo;
    }

    public void setGameExtraInfo(String gameExtraInfo) {
        this.mGameExtraInfo = gameExtraInfo;
    }

    public String getCityId() {
        return this.mCityId;
    }

    public void setCityId(String cityId) {
        this.mCityId = cityId;
    }

    public String getCountryCode() {
        return this.mCountryCode;
    }

    public void setCountryCode(String countryCode) {
        this.mCountryCode = countryCode;
    }

    public String getStateCode() {
        return this.mStateCode;
    }

    public void setStateCode(String stateCode) {
        this.mStateCode = stateCode;
    }

    public String getLocCityId() {
        return this.mLocCityId;
    }

    public void setLocCityId(String locCityId) {
        this.mLocCityId = locCityId;
    }
}
