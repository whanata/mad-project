package com.mad.gamerfriends.fragment.chat;

import android.content.Context;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.Query;
import com.mad.gamerfriends.R;
import com.mad.gamerfriends.adapter.ChatRecyclerAdapter;
import com.mad.gamerfriends.exception.FirebaseDbException;
import com.mad.gamerfriends.handler.ChatHandler;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;

/**
 * Fragment for chatting with another user.
 * @author Wirawan Tjo
 * @version 1.0
 */
public class ChatFragment extends Fragment {
    public static final String LOG_TAG = "ChatFragment";
    public static final String RECEIVER_ID_KEY = "Receiver Steam ID";

    @BindView(R.id.fragment_chat_messages_RecyclerView)
    protected RecyclerView mRecyclerView;
    @BindView(R.id.fragment_chat_message_ConstraintLayout)
    protected ConstraintLayout mMessageConstraintLayout;
    @BindView(R.id.fragment_chat_send_Button)
    protected Button mSendButton;
    @BindView(R.id.fragment_chat_message_EditText)
    protected EditText mMessageEditText;
    @BindView(R.id.fragment_chat_ProgressBar)
    protected ProgressBar mProgressBar;

    private ChatHandler mChatHandler;
    private CompositeDisposable mCompositeDisposable;
    private Unbinder mUnbinder;
    private Context mContext;
    private String mReceiverId;
    private ChatRecyclerAdapter mChatRecyclerAdapter;
    private LinearLayoutManager mLinearLayoutManager;

    /**
     * Create new instance of this fragment.
     * @param receiverId The chat recipient steam ID
     * @return ChatFragment instance
     */
    public static ChatFragment newInstance(String receiverId) {
        ChatFragment chatFragment = new ChatFragment();
        Bundle args = new Bundle();
        args.putString(ChatFragment.RECEIVER_ID_KEY, receiverId);
        chatFragment.setArguments(args);
        return chatFragment;
    }

    /**
     * Instantiate variables that doesn't require a view.
     * @param savedInstanceState Saved information from previous state
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mReceiverId = getArguments().getString(ChatFragment.RECEIVER_ID_KEY);
        this.mLinearLayoutManager = new LinearLayoutManager(this.mContext);
    }

    /**
     * Instantiate variables that require a view. This includes butterknife instance and
     * ChatHandler.
     * @param inflater Used to inflate the fragment's view on the activity
     * @param container The container where the fragment's view is inflated.
     * @param savedInstanceState Saved information from previous state
     * @return The view for this fragment.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat, container, false);
        this.mUnbinder = ButterKnife.bind(this, view);

        this.mCompositeDisposable = new CompositeDisposable();

        this.mChatHandler = new ChatHandler();

        this.mChatHandler.addReceiverUser(this.mReceiverId);

        this.mCompositeDisposable.add(
                this.mChatHandler.getThread(this.getChatThreadObserver()));
        return view;
    }

    /**
     * Clean up the ChatRecyclerAdapter so it doesn't leak.
     */
    @Override
    public void onDestroyView() {
        super.onDestroyView();
        this.mChatRecyclerAdapter.cleanup();
    }

    /**
     * Get context of the activity.
     * @param context The context of the activity containing this fragment.
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mContext = context;
    }

    /**
     * Clear any observers and unbind the butterknife instance.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mCompositeDisposable.clear();
        this.mUnbinder.unbind();
    }

    /**
     * Initialise the chat by showing the content of the RecyclerView for the chat. This runs
     * when all the information from the API is given to the app about the chat.
     */
    protected void initialiseChat() {
        Query query = this.mChatHandler.getRecyclerAdapterQuery();
        this.mChatRecyclerAdapter = new ChatRecyclerAdapter
                (this.mChatHandler, this.mLinearLayoutManager, R.layout.item_chat, query);
        this.mRecyclerView.setLayoutManager(this.mLinearLayoutManager);
        this.mRecyclerView.setAdapter(this.mChatRecyclerAdapter);
        this.mProgressBar.setVisibility(View.GONE);
        this.mRecyclerView.setVisibility(View.VISIBLE);
        this.mMessageConstraintLayout.setVisibility(View.VISIBLE);
        this.mLinearLayoutManager.scrollToPosition(this.mChatRecyclerAdapter.getItemCount());
    }

    /**
     * On clicking the send button, send a message - look at sendMessage method in ChatHandler
     * for the specification.
     */
    @OnClick(R.id.fragment_chat_send_Button)
    protected void sendAction() {
        String message = this.mMessageEditText.getText().toString();
        this.mChatHandler.sendMessage(message);
        this.mChatRecyclerAdapter.notifyDataSetChanged();
        this.mMessageEditText.setText("");
    }

    /**
     * Observer for getting ChatThread.
     * @return Observer for getting Chat Thread
     */
    private DisposableObserver<DataSnapshot> getChatThreadObserver() {
        return new DisposableObserver<DataSnapshot>() {

            /**
             * Get the result from the Observable and process it.
             * @param dataSnapshot The data from the Observable
             */
            @Override
            public void onNext(@NonNull DataSnapshot dataSnapshot) {
                Log.d(ChatFragment.LOG_TAG, "onNext: getChatThreadObserver");
                mChatHandler.getThreadResult(dataSnapshot);
            }

            /**
             * If the error is empty result, we should create a new chat thread and initialise the
             * thread.
             * @param e Exception
             */
            @Override
            public void onError(@NonNull Throwable e) {
                Log.e(ChatFragment.LOG_TAG, "onError: getChatThreadObserver", e);
                if (e.getMessage().contains(FirebaseDbException.EMPTY_RESULT)) {
                    mChatHandler.createNewThread();
                    initialiseChat();
                }
            }

            /**
             * Initialise the thread and show the chat to the user.
             */
            @Override
            public void onComplete() {
                Log.d(ChatFragment.LOG_TAG, "onComplete: getChatThreadObserver");
                mChatHandler.initialiseThread();
                initialiseChat();
            }
        };
    }
}
