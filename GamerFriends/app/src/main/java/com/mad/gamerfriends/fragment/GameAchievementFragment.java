package com.mad.gamerfriends.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mad.gamerfriends.R;
import com.mad.gamerfriends.activity.GameActivity;
import com.mad.gamerfriends.adapter.GameAchievementRecyclerAdapter;
import com.mad.gamerfriends.handler.GameHandler;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * Fragment showing Game Achievements for a specific game by a specific user.
 * This is in the GameActivity.
 * @author Wirawan Tjo
 * @version 1.0
 */
public class GameAchievementFragment extends Fragment {
    public static final String LOG_TAG = "GameAchievementFragment";

    @BindView(R.id.fragment_game_achievement_RecyclerView)
    protected RecyclerView mRecyclerView;

    private GameHandler mGameHandler;
    private Unbinder mUnbinder;
    private GameAchievementRecyclerAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    /**
     * Create new instance of this fragment.
     * @return GameAchievementFragment instance
     */
    public static GameAchievementFragment newInstance() {
        return new GameAchievementFragment();
    }

    /**
     * Get GameHandler from GameActivity
     * @param context The context of the activity containing this fragment.
     */
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        GameActivity activity = (GameActivity) context;
        this.mGameHandler = activity.getGameHandler();
    }

    /**
     * Initialise the variables from the fragment that requires view. Includes initialising
     * Butterknife and setting RecyclerView.
     * @param inflater Used to inflate the fragment's view on the activity
     * @param container The container where the fragment's view is inflated.
     * @param savedInstanceState Saved information from previous state
     * @return The view for this fragment.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_game_achievement, container, false);
        this.mUnbinder = ButterKnife.bind(this, view);

        this.mAdapter = new GameAchievementRecyclerAdapter(this.mGameHandler);
        this.mLayoutManager = new LinearLayoutManager(getActivity());
        this.mRecyclerView.setLayoutManager(this.mLayoutManager);
        this.mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        this.mRecyclerView.setAdapter(this.mAdapter);
        return view;
    }

    /**
     * GameHandler came from the parent activity so making it null ensures it doesn't leak out.
     */
    @Override
    public void onDetach() {
        super.onDetach();
        // So doesn't leak (mGameHandler comes from base activity)
        this.mGameHandler = null;
    }

    /**
     * Unbind Butterknife instance
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mUnbinder.unbind();
    }

}
