package com.mad.gamerfriends.handler;

import android.util.Log;

import com.mad.gamerfriends.api.SteamWebApi;
import com.mad.gamerfriends.model.Game;
import com.mad.gamerfriends.response.GameSchemaResponse;
import com.mad.gamerfriends.response.OwnedGameListResponse;
import com.mad.gamerfriends.response.OwnedGameResponse;
import com.mad.gamerfriends.response.UserGameStatResponse;

import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Handles all the activity associated owned game list of a user. This is used in GameListFragment
 * and ChatGameListFragment.
 * @author Wirawan Tjo
 * @version 1.0
 */
public class GameListHandler {
    public static final String LOG_TAG = "GameListHandler";

    public static final String SORT_BY_NAME = "Sort By Game Name";

    private UserHandler mUserHandler;
    private SteamWebApi mApi;

    /**
     * Constructor class to initialise the API and CurrentUserHandler.
     * This is used for the current user.
     */
    public GameListHandler() {
        this.mUserHandler = CurrentUserHandler.getInstance();
        this.mApi = SteamWebApi.getInstance();
    }

    /**
     * Constructor class to initialise the API and CurrentUserHandler.
     * This is used for a another user, other than specific user.
     * @param steamId The user's steam ID
     */
    public GameListHandler(String steamId) {
        this.mUserHandler = new UserHandler(steamId);
        this.mApi = SteamWebApi.getInstance();
    }

    /**
     * Get UserHandler
     * @return UserHandler Object
     */
    public UserHandler getUserHandler() {
        return this.mUserHandler;
    }

    /**
     * Get games that the user owns. Get OwnedGameList (List of App ID of games that the user owns).
     * Filter for each games to check if they have been played by the user. For each game,
     * do 2 API calls to get both the Game Schema and Game information for that specific user.
     * Do this in multiple threads.
     * @param observer Observer for this Disposable
     * @return Disposable for getting a game list
     */
    public Disposable getGameList(DisposableObserver<String> observer) {
        final String steamId = this.mUserHandler.getSteamId();
        return this.mApi.getOwnedGames(steamId)
                .flatMapIterable(new Function<OwnedGameListResponse, Iterable<OwnedGameResponse>>() {

                    @Override
                    public Iterable<OwnedGameResponse> apply(
                            @NonNull OwnedGameListResponse ownedGameListResponse) throws Exception {
                        return ownedGameListResponse.getGameList();
                    }
                })
                .filter(new Predicate<OwnedGameResponse>() {

                    @Override
                    public boolean test(@NonNull OwnedGameResponse ownedGameResponse) throws Exception {
                        return ownedGameResponse.getTotalPlaytime() > 0;
                    }
                })
                .flatMap(new Function<OwnedGameResponse, ObservableSource<String>>() {

                    @Override
                    public ObservableSource<String> apply(
                            final @NonNull OwnedGameResponse ownedGameResponse) throws Exception {
                        final String appId = ownedGameResponse.getAppId();
                        return Observable.zip(
                                mApi.getGameSchema(appId),
                                mApi.getUserGameStats(steamId, appId),
                                new BiFunction<GameSchemaResponse, UserGameStatResponse, String>() {

                                    @Override
                                    public String apply(
                                            @NonNull GameSchemaResponse gameSchemaResponse,
                                            @NonNull UserGameStatResponse userGameStatResponse)
                                            throws Exception {
                                        getGameToUser(ownedGameResponse,
                                                gameSchemaResponse,
                                                userGameStatResponse);

                                        return String.format("%s: %s", steamId, appId);
                                    }
                                })
                                .subscribeOn(Schedulers.io())
                                .onErrorResumeNext(
                                        new Function<Throwable, ObservableSource<String>>() {

                                            @Override
                                            public ObservableSource<String> apply(
                                                    @NonNull Throwable e) throws Exception {
                                                getGameToUser(ownedGameResponse);
                                                Log.e(GameListHandler.LOG_TAG, "onError: getGames", e);
                                                return Observable.just(
                                                        String.format("Error: %s: %s", steamId, appId));
                                            }
                                        });
                    }
                })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(observer);
    }

    /**
     * Add information from OwnedGameResponse (Steam API response) to the game object.
     * @param ownedGameResponse Response from OwnedGameList
     */
    private void getGameToUser(OwnedGameResponse ownedGameResponse) {
        Game game = new Game();
        game.addOwnedGameResponse(ownedGameResponse);
        this.mUserHandler.addGame(game);
    }

    /**
     * Add information from OwnedGameResponse, GameSchemaResponse, UserGameStatResponse to the game
     * object.
     * @param ownedGameResponse Response from OwnedGameList
     * @param gameSchemaResponse Response from GameSchema
     * @param userGameStatResponse Response from UserGameStats
     */
    private void getGameToUser(
            OwnedGameResponse ownedGameResponse,
            GameSchemaResponse gameSchemaResponse,
            UserGameStatResponse userGameStatResponse) {
        Game game = new Game();
        game.addOwnedGameResponse(ownedGameResponse);
        game.addGameAchievementStats(gameSchemaResponse, userGameStatResponse);
        this.mUserHandler.addGame(game);
    }

    /**
     * Choose the type of sort, for now it's just the name of the game
     * @param by
     */
    public void sortGameList(String by) {
        switch(by) {
            case GameListHandler.SORT_BY_NAME:
                List<Game> games = this.mUserHandler.getGameList();
                Collections.sort(games, Game.COMPARE_BY_NAME_ASC);
                break;
            default:
                break;
        }
    }
}
