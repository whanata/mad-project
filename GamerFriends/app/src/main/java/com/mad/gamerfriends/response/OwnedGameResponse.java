package com.mad.gamerfriends.response;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

/**
 * Created by whanata on 23/9/17.
 */

public class OwnedGameResponse {
    @SerializedName("appid")
    private String mAppId;
    @SerializedName("name")
    private String mName;
    @SerializedName("playtime_2weeks")
    private int mTwoWeeksPlaytime;
    @SerializedName("playtime_forever")
    private int mTotalPlaytime;
    @SerializedName("img_icon_url")
    private String mIconHash;
    @SerializedName("img_logo_url")
    private String mLogoHash;
    @SerializedName("has_community_visible_stats")
    private String mCommunityVisibility;

    public OwnedGameResponse() {
        this.mTwoWeeksPlaytime = 0;
        this.mTotalPlaytime = 0;
    }

    public OwnedGameResponse(
            String mAppId,
            String mName,
            int mTwoWeeksPlaytime,
            int mTotalPlaytime,
            String mIconHash,
            String mLogoHash,
            String mCommunityVisibility) {
        this.mAppId = mAppId;
        this.mName = mName;
        this.mTwoWeeksPlaytime = mTwoWeeksPlaytime;
        this.mTotalPlaytime = mTotalPlaytime;
        this.mIconHash = mIconHash;
        this.mLogoHash = mLogoHash;
        this.mCommunityVisibility = mCommunityVisibility;
    }

    public String getAppId() {
        return this.mAppId;
    }

    public void setAppId(String appId) {
        this.mAppId = appId;
    }

    public String getName() {
        return this.mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public int getTwoWeeksPlaytime() {
        return this.mTwoWeeksPlaytime;
    }

    public void setTwoWeeksPlaytime(int twoWeeksPlaytime) {
        this.mTwoWeeksPlaytime = twoWeeksPlaytime;
    }

    public int getTotalPlaytime() {
        return this.mTotalPlaytime;
    }

    public void setTotalPlaytime(int totalPlaytime) {
        this.mTotalPlaytime = totalPlaytime;
    }

    public String getIconHash() {
        return this.mIconHash;
    }

    public void setIconHash(String iconHash) {
        this.mIconHash = iconHash;
    }

    public String getLogoHash() {
        return this.mLogoHash;
    }

    public void setLogoHash(String logoHash) {
        this.mLogoHash = logoHash;
    }

    public String getCommunityVisibility() {
        return this.mCommunityVisibility;
    }

    public void setCommunityVisibility(String communityVisibility) {
        this.mCommunityVisibility = communityVisibility;
    }
}
