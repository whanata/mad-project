package com.mad.gamerfriends.service;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.mad.gamerfriends.api.SteamWebApi;
import com.mad.gamerfriends.db.FirebaseDb;
import com.mad.gamerfriends.model.User;
import com.mad.gamerfriends.response.UserResponse;
import com.mad.gamerfriends.response.UserSummaryResponse;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class UserService extends IntentService {
    public static final String LOG_TAG = "UserService";
    public static final String STEAM_ID_KEY = "Steam ID Key";
    public static final String BROADCAST_KEY = "UserService";

    private SteamWebApi mApi;
    private FirebaseDb mFirebaseDb;
    private CompositeDisposable mCompositeDisposable;

    public UserService() {
        super("UserService");
        this.mApi = SteamWebApi.getInstance();
        this.mCompositeDisposable = new CompositeDisposable();
        this.mFirebaseDb = FirebaseDb.getInstance();
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String steamId = intent.getStringExtra(UserService.STEAM_ID_KEY);
        this.mCompositeDisposable.add(this.getUser(steamId));
    }

    protected Disposable getUser(String steamId) {
        return mApi.getUserSummaries(steamId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(this.getUserObserver());
    }

    protected DisposableObserver<UserSummaryResponse> getUserObserver() {
        final Intent intent = new Intent(UserService.BROADCAST_KEY);
        return new DisposableObserver<UserSummaryResponse>() {

            @Override
            public void onNext(@NonNull UserSummaryResponse userSummaryResponse) {
                Log.d(UserService.LOG_TAG, "onNext: getUserFromApiObserver");
                List<UserResponse> users = userSummaryResponse.getUsers();
                if (users != null && users.size() == 1) {
                    UserResponse userResponse = users.get(0);
                    User user = new User(userResponse);
                    mFirebaseDb.addUser(user);
                }
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.e(UserService.LOG_TAG, "onError: getUserFromApiObserver", e);
                LocalBroadcastManager.getInstance(UserService.this).sendBroadcast(intent);
            }

            @Override
            public void onComplete() {
                Log.d(UserService.LOG_TAG, "onComplete: getUserFromApiObserver");
                LocalBroadcastManager.getInstance(UserService.this).sendBroadcast(intent);
            }
        };
    }
}
