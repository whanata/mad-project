package com.mad.gamerfriends.handler;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.Query;
import com.mad.gamerfriends.db.FirebaseDb;
import com.mad.gamerfriends.model.ChatMessage;
import com.mad.gamerfriends.model.ChatThread;
import com.mad.gamerfriends.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Handles all the activity associated with chatting with another user.
 * This is used in ChatFragment.
 * @author Wirawan Tjo
 * @version 1.0
 */
public class ChatHandler {
    public static final String LOG_TAG = "ChatHandler";

    private FirebaseDb mFirebaseDb;
    private CurrentUserHandler mCurrentUserHandler;
    private UserHandler mReceiverUserHandler;
    private ChatThread mChatThread;

    /**
     * Constructor class - initiate the variables.
     */
    public ChatHandler() {
        this.mFirebaseDb = FirebaseDb.getInstance();
        this.mCurrentUserHandler = CurrentUserHandler.getInstance();
        this.mChatThread = new ChatThread();
    }

    /**
     * Get receiver ID, the user that the current user is talking to.
     * @return Receiver's Steam ID
     */
    public String getReceiverId() {
        return this.mReceiverUserHandler.getSteamId();
    }

    /**
     * Add ReceiverUser as a member variable to this handler.
     * @param receiverId Steam ID of Receiver
     */
    public void addReceiverUser(String receiverId) {
        User receiverUser = this.mCurrentUserHandler.getUserFromFriendList(receiverId);
        this.mReceiverUserHandler = new UserHandler(receiverUser);
    }

    /**
     * Get the Query of getting a chat thread.
     * @return Query of getting a specific chat thread.
     */
    public Query getRecyclerAdapterQuery() {
        return this.mFirebaseDb.getChatQuery(this.mChatThread.getThreadId());
    }

    /**
     * Disposable for getting chat threads from Firebase DB.
     * @param observer The observer from the activity.
     * @return Disposable for getting chat threads
     */
    public Disposable getThread(DisposableObserver<DataSnapshot> observer) {
        return this.mFirebaseDb.getChatMembers(
                this.mCurrentUserHandler.getSteamId())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(observer);
    }

    /**
     * Process a DataSnapshot to check if the chat thread has both the current user and receiver.
     * If the chat thread hasn't been found, check the chat thread of the members and check if the
     * receiver's Steam ID is in there. If not, do nothing. If they are, set the threadID to this
     * class.
     * @param dataSnapshot
     */
    public void getThreadResult(DataSnapshot dataSnapshot) {
        if (this.mChatThread.getThreadId() == null) {
            String threadId = dataSnapshot.getKey();
            @SuppressWarnings("unchecked")
            Map<String, String> result = (Map<String, String>) dataSnapshot.getValue();
            List<String> steamIds = new ArrayList<>(result.keySet());

            for (String steamId : steamIds) {
                if (steamId.equals(this.mReceiverUserHandler.getSteamId())) {
                    this.mChatThread.setThreadId(threadId);
                }
            }
        }
    }

    /**
     * Create new thread in Firebase DB between the current user and receiver.
     */
    public void createNewThread() {
        String threadId = this.mFirebaseDb.addChatThread(
                this.mCurrentUserHandler.getSteamId(), this.mReceiverUserHandler.getSteamId());
        this.mChatThread.setThreadId(threadId);
    }

    /**
     * Create new thread if there is no existing thread, if not don't do anything since it has
     * already been initialised.
     */
    public void initialiseThread() {
        if (this.mChatThread.getThreadId() == null) {
            createNewThread();
        }
    }

    /**
     * Create a ChatMessage Object and set the time, steam ID of user who send the message and
     * the message text itself. Add the ChatMessage Object to Firebase DB.
     * @param message
     */
    public void sendMessage(String message) {
        ChatMessage chatMessage = new ChatMessage();
        chatMessage.setTimestamp(Long.toString(System.currentTimeMillis()));
        chatMessage.setSteamId(mCurrentUserHandler.getSteamId());
        chatMessage.setText(message);
        this.mFirebaseDb.addChatMessage(this.mChatThread.getThreadId(), chatMessage);
    }

    /**
     * Get Username of Receiver.
     * @return Username
     */
    public String getReceiverUsername() {
        return this.mReceiverUserHandler.getUsername();
    }

    /**
     * Get Avatar URL of Receiver.
     * @return URL for the Avatar
     */
    public String getReceiverAvatarUrl() {
        return this.mReceiverUserHandler.getAvatarUrl();
    }
}
