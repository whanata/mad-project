package com.mad.gamerfriends.model;

import com.google.firebase.database.Exclude;
import com.mad.gamerfriends.response.GameStatResponse;
import com.mad.gamerfriends.response.UserStatResponse;

import java.util.Comparator;

/**
 * Game Stat for a specific user on a specific game.
 * @author Wirawan Tjo
 * @version 1.0
 */
public class GameStat {
    /**
     * A comparator constant for ordering name ascending.
     * Make sure that if there isn't a name present user the ID.
     */
    public static Comparator<GameStat> COMPARE_BY_NAME_ASC = new Comparator<GameStat>() {
        @Override
        public int compare(GameStat stat1, GameStat stat2) {
            String name1;
            String name2;

            if (!stat1.getName().equals("")) {
                name1 = stat1.getName();
            } else {
                name1 = stat1.getId();
            }

            if (!stat2.getName().equals("")) {
                name2 = stat2.getName();
            } else {
                name2 = stat2.getId();
            }
            return name1.toLowerCase().compareTo(name2.toLowerCase());
        }
    };

    private String mId;
    private String mName;
    private String mDefaultValue;
    private double mValue;

    public GameStat() {
        this.mValue = 0;
    }

    /**
     * Copy information from GameStatResponse to the game.
     * @param response GameStatResponse
     */
    public GameStat(GameStatResponse response) {
        this();
        this.mId = response.getId();
        this.mName = response.getName();
        this.mDefaultValue = response.getDefaultValue();
    }

    /**
     * Set stat data in this.mValue.
     * @param response UserStatResponse
     */
    public void addUserStatResponse(UserStatResponse response) {
        this.mValue = Double.parseDouble(response.getValue());
    }

    public String getId() {
        return this.mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getName() {
        return this.mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getDefaultValue() {
        return this.mDefaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.mDefaultValue = defaultValue;
    }

    @Exclude
    public double getValue() {
        return this.mValue;
    }

    public void setValue(double value) {
        this.mValue = value;
    }
}
