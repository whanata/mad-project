package com.mad.gamerfriends.fragment;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.firebase.database.DataSnapshot;
import com.mad.gamerfriends.R;
import com.mad.gamerfriends.adapter.FriendListRecyclerAdapter;
import com.mad.gamerfriends.handler.CurrentUserHandler;
import com.mad.gamerfriends.handler.UserHandler;
import com.mad.gamerfriends.service.FriendListService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;

/**
 * Fragment for the friend list of the current user.
 * @author Wirawan Tjo
 * @version 1.0
 */
public class FriendListFragment extends Fragment {
    public static final String LOG_TAG = "FriendListFragment";
    public static final String STEAM_ID_KEY = "Steam ID";

    @BindView(R.id.fragment_main_friend_list_RecyclerView)
    protected RecyclerView mRecyclerView;
    @BindView(R.id.fragment_main_friend_list_ProgressBar)
    protected ProgressBar mProgressBar;

    private CurrentUserHandler mCurrentUserHandler;
    private CompositeDisposable mCompositeDisposable;
    private Unbinder mUnbinder;

    private GetFriendListReceiver mGetFriendListReceiver;

    private FriendListRecyclerAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    /**
     * Create new instance of this fragment.
     * @return FriendListFragment instance
     */
    public static FriendListFragment newInstance() {
        return new FriendListFragment();
    }

    /**
     * Instantiate variables that doesn't require a view. This includes the CurrentUserHandler.
     * @param savedInstanceState Saved information from previous state
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mCompositeDisposable = new CompositeDisposable();
        this.mCurrentUserHandler = CurrentUserHandler.getInstance();
    }

    /**
     * Instantiate variables that require a view. This includes butterknife instance and
     * ChatHandler. It also registers the ServiceBroadcast to listen to any broadcast messages
     * from the IntentService (FriendListService).
     * @param inflater Used to inflate the fragment's view on the activity
     * @param container The container where the fragment's view is inflated.
     * @param savedInstanceState Saved information from previous state
     * @return The view for this fragment.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_friend_list, container, false);
        this.mUnbinder = ButterKnife.bind(this, view);

        this.registerServiceBroadcast();

        this.mAdapter = new FriendListRecyclerAdapter(getActivity());
        this.mLayoutManager = new LinearLayoutManager(getActivity());
        this.mRecyclerView.setLayoutManager(this.mLayoutManager);
        this.mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        this.mRecyclerView.setAdapter(this.mAdapter);

        return view;
    }

    /**
     * Unregister the broadcast receiver when it is not on the fragment.
     */
    @Override
    public void onPause() {
        super.onPause();
        this.unregisterServiceBroadcast();
    }

    /**
     * Clear any observers and unbind the butterknife instance.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mCompositeDisposable.clear();
        this.mUnbinder.unbind();
    }

    /**
     * Add BroadcaseReceiver to look for any broadcase from FriendListService.
     */
    private void registerServiceBroadcast() {
        this.mGetFriendListReceiver = new GetFriendListReceiver();

        LocalBroadcastManager.getInstance(getActivity()).registerReceiver(
                this.mGetFriendListReceiver, new IntentFilter(FriendListService.BROADCAST_KEY));
    }

    /**
     * Unregister the BroadcastReceiver so it won't continuously looking for Broadcast if not
     * needed.
     */
    private void unregisterServiceBroadcast() {
        LocalBroadcastManager.getInstance(getActivity()).unregisterReceiver(
                this.mGetFriendListReceiver);
    }

    /**
     * Remove ProgressBar and show the RecyclerView of the FriendList. Also sort the FriendList
     * by username.
     */
    private void updateFriendList() {
        Log.d(FriendListFragment.LOG_TAG, "Update FriendList");
        this.mProgressBar.setVisibility(View.GONE);
        this.mCurrentUserHandler.sortFriendList(UserHandler.SORT_BY_NAME);
        this.mRecyclerView.setVisibility(View.VISIBLE);
        this.mAdapter.notifyDataSetChanged();
    }

    /**
     * The class to receive the broadcast from FriendListService. It will initiate an Observable to
     * get FriendList from Firebase DB.
     */
    public class GetFriendListReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(FriendListFragment.LOG_TAG, "FriendListReceiver: onReceive");
            mCompositeDisposable.add(
                    mCurrentUserHandler.getFriendList(getFriendListFromDbObserver()));
        }
    }

    /**
     * Observer to get FriendList
     * @return Observer for getting Friend List of current user
     */
    protected DisposableObserver<DataSnapshot> getFriendListFromDbObserver() {
        return new DisposableObserver<DataSnapshot>() {

            /**
             * Process the result of the FriendList (DataSnapshot)
             * @param dataSnapshot The result from FirebaseDB call
             */
            @Override
            public void onNext(@NonNull DataSnapshot dataSnapshot) {
                Log.d(FriendListFragment.LOG_TAG, "onNext: getFriendListFromDbObserver");
                mCurrentUserHandler.getFriendListResult(dataSnapshot);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.e(FriendListFragment.LOG_TAG, "onError: getFriendListFromDbObserver", e);
            }

            /**
             * Show the FriendList in the UI of the user when this is done.
             */
            @Override
            public void onComplete() {
                Log.d(FriendListFragment.LOG_TAG, "onComplete: getFriendListFromDbObserver");
                updateFriendList();
            }
        };
    }
}
