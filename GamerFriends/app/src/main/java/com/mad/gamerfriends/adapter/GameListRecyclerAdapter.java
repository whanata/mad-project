package com.mad.gamerfriends.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;
import com.mad.gamerfriends.R;
import com.mad.gamerfriends.activity.GameActivity;
import com.mad.gamerfriends.handler.CurrentUserHandler;
import com.mad.gamerfriends.handler.UserHandler;
import com.mad.gamerfriends.model.Game;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Adapter Class for the GameListFragment RecyclerView. Shows the Owned Game List for current user.
 * @author  Wirawan Tjo
 * @version 1.0
 */
public class GameListRecyclerAdapter extends RecyclerView.Adapter<GameListRecyclerAdapter.ViewHolder> {
    public static final String LOG_TAG = "GamesListRecyclerAdapter";

    private Context mContext;
    private UserHandler mUserHandler;

    /**
     * Instantiate GameListRecyclerAdapter (for current user)
     * @param context Context where this adapter is instantiated.
     */
    public GameListRecyclerAdapter(Context context) {
        this.mContext = context;
        this.mUserHandler = CurrentUserHandler.getInstance();
    }

    /**
     * Instantiate GameListRecyclerAdapter (for specific user - not current user)
     * @param context Context where this adapter is instantiated.
     */
    public GameListRecyclerAdapter(Context context, UserHandler userHandler) {
        this.mContext = context;
        this.mUserHandler = userHandler;
    }

    /**
     * Inner class - Represents each item in the RecyclerView. Each instance of this class is one
     * item in the RecyclerView.
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        public static final String STEAM_ID_KEY = "Steam ID";
        public static final String APP_ID_KEY = "App ID";

        @BindView(R.id.item_game_image_ImageView)
        protected ImageView mGameItemProfileImageView;
        @BindView(R.id.item_game_name_TextView)
        protected TextView mGameItemUsernameTextView;

        private String mAppId;

        /**
         * The constructor for this class.
         * Gets all the views (widgets and layout) for this each row of the list of games.
         * @param itemView The Item View for this ViewHolder
         */
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.mAppId = "";
        }

        /**
         * Sets the Game Information to the item layout.
         * @param game Game for this ViewHolder
         */
        public void setGameInfo(Game game) {
            this.mAppId = game.getAppId();
            Ion.with(this.mGameItemProfileImageView)
                    .placeholder(R.drawable.ic_placeholder_profile)
                    .error(R.drawable.ic_placeholder_profile)
                    .animateIn(R.anim.fade_in)
                    .load(game.getIconUrl());

            this.mGameItemUsernameTextView.setText(game.getName());
        }

        /**
         * On clicking the main body of the item, start GameActivity for this user.
         */
        @OnClick(R.id.item_game_body_ConstraintLayout)
        public void showGameAction() {
            Intent gameIntent = new Intent(mContext, GameActivity.class);
            gameIntent.putExtra(ViewHolder.STEAM_ID_KEY, mUserHandler.getSteamId());
            gameIntent.putExtra(ViewHolder.APP_ID_KEY, this.mAppId);
            mContext.startActivity(gameIntent);
        }
    }

    /**
     * Create a single ViewHolder.
     * @param parent The parent view for the adapter
     * @param viewType Type of view
     * @return new ViewHolder Object
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_game, parent, false);
        return new ViewHolder(itemView);
    }

    /**
     * Bind ViewHolder to a Game Object
     * @param holder The ViewHolder Object.
     * @param position Position of the ViewHolder in context of the RecylerView
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Game game = this.getGameList().get(position);
        holder.setGameInfo(game);
    }

    /**
     * Count of the rows in the RecyclerView.
     * @return Count of rows in the RecyclerView.
     */
    @Override
    public int getItemCount() {
        return this.getGameList().size();
    }

    /**
     * Get GameList used by this adapter.
     * @return GameList used for this adapter.
     */
    private List<Game> getGameList() {
        return this.mUserHandler.getGameList();
    }
}
