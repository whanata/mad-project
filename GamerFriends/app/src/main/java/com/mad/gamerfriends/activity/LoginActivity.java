package com.mad.gamerfriends.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.mad.gamerfriends.R;
import com.mad.gamerfriends.login.FirebaseLogin;
import com.mad.gamerfriends.login.SteamLogin;
import com.mad.gamerfriends.service.FriendListService;
import com.mad.gamerfriends.service.UserService;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

/**
 * The activity for logging in through Steam to the application.
 * @author  Wirawan Tjo
 * @version 1.0
 */
public class LoginActivity extends AppCompatActivity {
    public static final String LOG_TAG = "LoginActivity";

    @BindView(R.id.activity_login_steam_Button)
    protected Button mSteamLoginButton;
    @BindView(R.id.activity_login_loading_ProgressBar)
    protected ProgressBar mLoadingProgressBar;
    @BindView(R.id.activity_login_steam_WebView)
    protected WebView mSteamWebView;
    @BindView(R.id.activity_login_no_connection_TextView)
    protected TextView mNoConnectionTextView;

    private SteamLogin mSteamLogin;
    private FirebaseLogin mFirebaseLogin;
    private Unbinder mUnbinder;

    /**
     * Instantiate LoginActivity - run this during a new instantiation of ChatActivity. Checks if
     * network is available, if not show message there is no network, if there is, run normal
     * login.
     * @param savedInstanceState Saved information from previous state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        this.mUnbinder = ButterKnife.bind(this);

        if (this.isNetworkAvailable()) {
            this.mSteamLogin = new SteamLogin(this);
            this.mFirebaseLogin = FirebaseLogin.getInstance();

            String steamId = this.mSteamLogin.getUserId();
            if (steamId != null) {
                this.goToNextActivity(steamId);
            }
        } else {
            this.showNoNetwork();
        }
    }

    /**
     * Unbind butterknife instance during onDestroy.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.mUnbinder.unbind();
    }

    /**
     * Cicking on the Login button shows the ProgressBar while it creates the WebView for logging in
     * Steam
     */
    @OnClick(R.id.activity_login_steam_Button)
    protected void steamLoginAction() {
        this.mSteamLoginButton.setVisibility(View.GONE);
        this.mLoadingProgressBar.setVisibility(View.VISIBLE);
        this.createSteamLoginWebView();
    }

    /**
     * Show TextView for no network
     */
    protected void showNoNetwork() {
        this.mNoConnectionTextView.setVisibility(View.VISIBLE);
        this.mSteamLoginButton.setVisibility(View.GONE);
    }

    /**
     * Check network availability
     * @return If there is an internet connection or not
     */
    protected boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /**
     * Inner Class - Create WebView for Logging into Steam.
     */
    protected void createSteamLoginWebView() {
        String url = this.mSteamLogin.getUrl();
        Log.d(LoginActivity.LOG_TAG, url);

        this.mSteamWebView.getSettings().setJavaScriptEnabled(true);

        this.mSteamWebView.loadUrl(url);

        this.mSteamWebView.setWebViewClient(new WebViewClient() {

            /**
             * Whenever a page is loaded, check if there is a ProgressBar, if there is, remove
             * Progress bar and show the WebView (Only for the first page it loads).
             * @param view The WebView
             * @param url The URL the page finish loading on.
             */
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                // Remove progressbar and show page
                if (mLoadingProgressBar.getVisibility() == View.VISIBLE) {
                    mLoadingProgressBar.setVisibility(View.GONE);
                    mSteamWebView.setVisibility(View.VISIBLE);
                }
            }

            /**
             * Check if URL is the URL that was specified for Steam Login. If the URL is specified,
             * close webview and login to Firebase. Also it will go to the next Activity which is
             * the MainActivity.
             * @param view The WebView
             * @param url The Page URL it is loading.
             * @param favicon The image for the WebView icon on the corner.
             */
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                // Get User ID if the redirected URL (After authentication) is the same as the
                // redirected URL stated
                Uri returnUrl = Uri.parse(url);
                String authority = returnUrl.getAuthority();
                if (authority != null) {
                    if (authority.equals(SteamLogin.REALM_PARAM)) {
                        this.closeWebView();

                        String steamId = mSteamLogin.getUserId(returnUrl);

                        goToNextActivity(steamId);
                    }
                }
            }

            /**
             * Close WebView Completely.
             */
            private void closeWebView() {
                mSteamWebView.stopLoading();
                mSteamWebView.loadData("", "text/html", null);
                mSteamWebView.pauseTimers();
                mSteamWebView.getSettings().setJavaScriptEnabled(false);
                mSteamWebView.setVisibility(View.GONE);
            }
        });
    }

    /**
     * Go to next activity - MainActivity. Also closes this current activity.
     *
     * @param steamId The steam ID of the user logging in.
     */
    protected void goToNextActivity(String steamId) {
        // Authorize user (Firebase Auth)
        this.mFirebaseLogin.authorizeUser(steamId);
        Intent nextIntent = new Intent(getBaseContext(), MainActivity.class);
        startActivity(nextIntent);
        finish();
    }
}
