package com.mad.gamerfriends.exception;

/**
 * Created by whanata on 16/9/17.
 */

public class FirebaseDbException extends Exception {
    public static final String EMPTY_RESULT = "DB Result is empty";

    public FirebaseDbException(String path, String message) {
        super(String.format("%s: %s", path, message));
    }

    public FirebaseDbException(Throwable e) {
        super(e);
    }

    public FirebaseDbException(String path, Throwable e) {
        super(path, e);
    }

    public FirebaseDbException(String path, String message, Throwable cause) {
        super(String.format("%s: %s", path, message), cause);
    }
}
