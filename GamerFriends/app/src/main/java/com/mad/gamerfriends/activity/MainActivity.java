package com.mad.gamerfriends.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.mad.gamerfriends.R;
import com.mad.gamerfriends.fragment.FriendListFragment;
import com.mad.gamerfriends.fragment.GameListFragment;
import com.mad.gamerfriends.fragment.SettingFragment;
import com.mad.gamerfriends.handler.CurrentUserHandler;
import com.mad.gamerfriends.helper.PreferenceHelper;
import com.mad.gamerfriends.login.SteamLogin;
import com.mad.gamerfriends.service.FriendListService;
import com.mad.gamerfriends.service.UserService;
import com.roughike.bottombar.BottomBar;
import com.roughike.bottombar.OnTabSelectListener;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;

/**
 * The activity for all the main tasks. It provides 3 fragments FriendListFragment,
 * GameListFragment and SettingFragment. This basically shows the Friend List, Game List and Setting
 * which contains logging out.
 * @author  Wirawan Tjo
 * @version 1.0
 */
public class MainActivity extends AppCompatActivity {
    public static final String LOG_TAG = "MainActivity";

    @BindView(R.id.activity_main_ProgressBar)
    ProgressBar mProgressBar;
    @BindView(R.id.activity_main_ViewPager)
    ViewPager mViewPager;
    @BindView(R.id.activity_main_navigation_BottomBar)
    BottomBar mNavigationBottomBar;
    @BindView(R.id.activity_main_no_connection_TextView)
    TextView mNoConnectionTextView;

    private Unbinder mUnbinder;
    private CurrentUserHandler mCurrentUserHandler;
    private CompositeDisposable mCompositeDisposable;
    private MainPagerAdapter mMainPagerAdapter;
    private GetUserReceiver mGetUserReceiver;

    /**
     * Instantiate MainActivity - run this during a new instantiation of GameActivity. Also checks
     * if internet exists. If it doesn't, it will just show a TextView saying there is no internet.
     * @param savedInstanceState Saved information from previous state
     */
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.mUnbinder = ButterKnife.bind(this);

        // This is because preference needs the activity
        PreferenceHelper preference = new PreferenceHelper(this);
        String steamId = preference.getString(SteamLogin.STEAM_ID_KEY);

        this.startService(steamId);

        this.mCurrentUserHandler = CurrentUserHandler.getInstance();
        this.mCurrentUserHandler.setSteamId(steamId);

        this.mCompositeDisposable = new CompositeDisposable();

        if (this.isNetworkAvailable()) {
            this.registerServiceBroadcast();

            this.mMainPagerAdapter = new MainPagerAdapter(getSupportFragmentManager());
            this.mViewPager.setAdapter(this.mMainPagerAdapter);

            this.addBottomBarListener();
            this.addViewPagerOnPageChangeListener();
        }
        else {
            this.showNoNetwork();
        }
    }

    /**
     * This governs the action for pressing back on the bottom of the screen for this activity.
     * Basically, if it's in the FriendListFragment, it will exit the app,
     * otherwise go to FriendListFragment.
     */
    @Override
    public void onBackPressed() {
        if (this.mViewPager.getCurrentItem() == 0) {
            super.onBackPressed();
        } else {
            this.mViewPager.setCurrentItem(0);
        }
    }

    /**
     * Unbind Butterknife Instance, unregister the ServiceBroadcastListener and
     * remove the observers when activity is destroyed.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.mUnbinder.unbind();
        this.unregisterServiceBroadcast();
        this.mCompositeDisposable.clear();
    }

    /**
     * Starts the 2 IntentServices which are used to get Steam Information for the current user
     * logging in and their friends from the Steam API.
     * @param steamId The steam ID of the user logging in.
     */
    private void startService(String steamId) {
        Intent getUserIntent = new Intent(this, UserService.class);
        getUserIntent.putExtra(UserService.STEAM_ID_KEY, steamId);
        startService(getUserIntent);

        Intent getFriendListIntent = new Intent(this, FriendListService.class);
        getFriendListIntent.putExtra(FriendListService.STEAM_ID_KEY, steamId);
        startService(getFriendListIntent);
    }

    /**
     * Register Broadcast Receiver for the UserService.
     */
    private void registerServiceBroadcast() {
        this.mGetUserReceiver = new GetUserReceiver();

        LocalBroadcastManager.getInstance(this).registerReceiver(
                this.mGetUserReceiver, new IntentFilter(UserService.BROADCAST_KEY));
    }

    /**
     * Unregister the Broadcase Receiver for the UserService.
     */
    private void unregisterServiceBroadcast() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(
                this.mGetUserReceiver);
    }

    /**
     * Inner class - BroadcastReceiver for UserService. Also creates an Observable for getting the
     * user from FirebaseDB since the service has dumped User information from Steam API to
     * Firebase DB.
     */
    public class GetUserReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            mCompositeDisposable.add(
                    mCurrentUserHandler.getUser(getUserObserver()));
        }
    }

    /**
     * Show the FragmentViewPager, remove the ProgressBar.
     */
    protected void showFragments() {
        this.mProgressBar.setVisibility(View.GONE);
        this.mViewPager.setVisibility(View.VISIBLE);
    }

    /**
     * Show TextView for no network
     */
    protected void showNoNetwork() {
        this.mNoConnectionTextView.setVisibility(View.VISIBLE);
        this.mProgressBar.setVisibility(View.GONE);
    }

    /**
     * Check network availability
     * @return If there is an internet connection or not
     */
    protected boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /**
     * Add listener to check what fragment the ViewPager is on which will change the BotttomBar
     * navigation on the bottom of the screen.
     */
    protected void addViewPagerOnPageChangeListener() {
        ViewPager.OnPageChangeListener listener = new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            /**
             * On the current page, make the Bottom Bar Navigation on the correct position also.
             * @param position
             */
            @Override
            public void onPageSelected(int position) {
                mNavigationBottomBar.setDefaultTabPosition(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        };

        this.mViewPager.addOnPageChangeListener(listener);
    }

    /**
     * Observer to get user information from DB to CurrentUserHandler singleton.
     * @return Disposable Observer used with getUser method
     */
    protected DisposableObserver<DataSnapshot> getUserObserver() {
        return new DisposableObserver<DataSnapshot>() {

            /**
             * Process the DataSnapshow, then show the fragments. Should only expect this method
             * to be called once only.
             * @param dataSnapshot The DataSnapshot from Firebase DB.
             */
            @Override
            public void onNext(@NonNull DataSnapshot dataSnapshot) {
                Log.d(MainActivity.LOG_TAG, "onNext: getUserObserver");
                mCurrentUserHandler.getUserResult(dataSnapshot);
                showFragments();
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.e(MainActivity.LOG_TAG, "onError: getUserObserver", e);
            }

            @Override
            public void onComplete() {
                Log.d(MainActivity.LOG_TAG, "onComplete: getUserObserver");
            }
        };
    }

    /**
     * Listener to check if BottomBar is pressed and change this to the correct fragment.
     */
    protected void addBottomBarListener() {
        OnTabSelectListener listener = new OnTabSelectListener() {

            @Override
            public void onTabSelected(@IdRes int tabId) {
                switch (tabId) {
                    case R.id.tab_home:
                        mViewPager.setCurrentItem(0);
                        break;
                    case R.id.tab_game:
                        mViewPager.setCurrentItem(1);
                        break;
                    case R.id.tab_setting:
                        mViewPager.setCurrentItem(2);
                        break;
                    default:
                        break;
                }
            }
        };

        this.mNavigationBottomBar.setOnTabSelectListener(listener);
    }

    /**
     * Inner class - Adapter for the ViewPager. It allows us to display multiple fragments in one
     * activity. This will show 3 fragments, FriendListFragment, GameListFragment and
     * SettingFragment.
     */
    public class MainPagerAdapter extends FragmentPagerAdapter {
        private List<Fragment> mFragments;

        /**
         * Instantiates a new MainActivity Pager Adapter.
         * @param fragmentManager The Fragment Manager
         */
        public MainPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
            this.mFragments = new ArrayList<>();
            this.mFragments.add(FriendListFragment.newInstance());
            this.mFragments.add(GameListFragment.newInstance());
            this.mFragments.add(SettingFragment.newInstance());
        }

        /**
         * Get a fragment.
         * @param position Position of the fragment.
         * @return Fragment from this.mFragment
         */
        @Override
        public Fragment getItem(int position) {
            return this.mFragments.get(position);
        }

        /**
         * Get number of Fragments in adapter.
         * @return Number of Fragments
         */
        @Override
        public int getCount() {
            return mFragments.size();
        }
    }
}
