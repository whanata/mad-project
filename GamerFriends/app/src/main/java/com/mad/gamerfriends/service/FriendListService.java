package com.mad.gamerfriends.service;

import android.app.IntentService;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.mad.gamerfriends.api.SteamWebApi;
import com.mad.gamerfriends.db.FirebaseDb;
import com.mad.gamerfriends.model.User;
import com.mad.gamerfriends.response.UserResponse;
import com.mad.gamerfriends.response.UserSummaryResponse;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Intent Service to get FriendList information from the Steam API for a specific user.
 * @author Wirawan Tjo
 * @version 1.0
 */
public class FriendListService extends IntentService {
    public static final String LOG_TAG = "FriendListService";
    public static final String STEAM_ID_KEY = "Steam ID Key";
    public static final String BROADCAST_KEY = "FriendListService";

    private SteamWebApi mApi;
    private FirebaseDb mFirebaseDb;
    private CompositeDisposable mCompositeDisposable;
    private String currentUserId;

    /**
     * Constructor for this intent service. Get API, Composite Disposable and FirebaseDB
     */
    public FriendListService() {
        super("FriendListService");
        this.mApi = SteamWebApi.getInstance();
        this.mCompositeDisposable = new CompositeDisposable();
        this.mFirebaseDb = FirebaseDb.getInstance();
    }

    /**
     * Get Steam ID from the intent and run disposable to get friend list from Steam API
     * @param intent Intent that starts the service
     */
    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String steamId = intent.getStringExtra(FriendListService.STEAM_ID_KEY);
        this.currentUserId = steamId;
        this.mCompositeDisposable.add(this.getFriendList(steamId));
    }

    /**
     * Disposable to get Friend List from Steam API
     * @param steamId Steam ID of the associated Friend List
     * @return Disposable to get Friend List
     */
    protected Disposable getFriendList(String steamId) {
        return mApi.getFriendList(steamId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(this.getFriendListObserver());
    }

    /**
     * Observer to get Friend List
     * @return Observer to get Friend List
     */
    protected DisposableObserver<UserSummaryResponse> getFriendListObserver() {
        final Intent intent = new Intent(FriendListService.BROADCAST_KEY);
        return new DisposableObserver<UserSummaryResponse>() {

            /**
             * Get one User (A friend) Summary.
             * Add the friend's steam ID in Firebase DB.
             * Add the user information in Firebase DB.
             * @param userSummaryResponse Response from UserSummary
             */
            @Override
            public void onNext(@NonNull UserSummaryResponse userSummaryResponse) {
                Log.d(FriendListService.LOG_TAG, "onNext: getFriendListObserver");
                List<UserResponse> users = userSummaryResponse.getUsers();
                for (UserResponse userResponse : users) {
                    User user = new User(userResponse);
                    mFirebaseDb.addUser(user);
                    mFirebaseDb.addFriend(currentUserId, user.getSteamId());
                }
            }

            /**
             * Send Broadcast.
             * @param e Exception
             */
            @Override
            public void onError(@NonNull Throwable e) {
                Log.e(FriendListService.LOG_TAG, "onError: getFriendListObserver", e);
                LocalBroadcastManager.getInstance(FriendListService.this).sendBroadcast(intent);
            }

            /**
             * Send Broadcast.
             */
            @Override
            public void onComplete() {
                Log.d(FriendListService.LOG_TAG, "onComplete: getFriendListObserver");
                LocalBroadcastManager.getInstance(FriendListService.this).sendBroadcast(intent);
            }
        };
    }
}
