package com.mad.gamerfriends.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.gigamole.navigationtabstrip.NavigationTabStrip;
import com.mad.gamerfriends.fragment.chat.ChatFragment;
import com.mad.gamerfriends.R;
import com.mad.gamerfriends.adapter.FriendListRecyclerAdapter;
import com.mad.gamerfriends.fragment.chat.ChatGameListFragment;
import com.mad.gamerfriends.handler.ChatHandler;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

/**
 * The chat activity for when user wants to chat with a user. It has 2 fragments, one for chatting
 * the other for looking at their games.
 * @author  Wirawan Tjo
 * @version 1.0
 */
public class ChatActivity extends AppCompatActivity {
    public static final String LOG_TAG = "ChatActivity";

    @BindView(R.id.activity_chat_ViewPager)
    protected ViewPager mViewPager;
    @BindView(R.id.activity_chat_NavigationTabStrip)
    protected NavigationTabStrip mNavigationTabStrip;

    private ChatHandler mChatHandler;
    private Unbinder mUnbinder;
    private ChatPagerAdapter mChatPagerAdapter;

    /**
     * Create the ChatActivity - this is run during the creation of a new activity
     * @param savedInstanceState Saved information from previous state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        this.mUnbinder = ButterKnife.bind(this);

        this.mChatHandler = new ChatHandler();

        this.mChatHandler.addReceiverUser(this.getReceiverId());
        this.showReceiverUsername();

        this.mChatPagerAdapter = new ChatPagerAdapter(getSupportFragmentManager());
        this.mViewPager.setAdapter(this.mChatPagerAdapter);
        this.mNavigationTabStrip.setViewPager(this.mViewPager, 0);
    }

    /**
     * This runs when activity gets destroyed. Used to destroy the Butterknife instance since
     * it will leak the activity if not destroyed.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.mUnbinder.unbind();
    }

    /**
     * Get Receiver Steam ID of the receiver of this chat. This is obtained from an intent from the
     * FriendList fragment.
     *
     * @return the receiver id
     */
    protected String getReceiverId() {
        Intent receiveIntent = getIntent();
        return receiveIntent.getStringExtra(
                FriendListRecyclerAdapter.ViewHolder.RECEIVER_ID_KEY);
    }

    /**
     * Show receiver username. This is obtained from the ChatHandler
     */
    protected void showReceiverUsername() {
        String username = this.mChatHandler.getReceiverUsername();
        setTitle(username);
    }

    /**
     * Inner class - Adapter for the ViewPager. It allows us to display multiple fragments in one
     * activity. This will show 2 fragments, ChatFragment and ChatGameListFragment
     */
    public class ChatPagerAdapter extends FragmentPagerAdapter {
        private List<Fragment> mFragments;

        /**
         * Instantiates a new Chat pager adapter.
         *
         * @param fragmentManager The Fragment Manager
         */
        public ChatPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
            this.mFragments = new ArrayList<>();
            this.mFragments.add(ChatFragment.newInstance(mChatHandler.getReceiverId()));
            this.mFragments.add(ChatGameListFragment.newInstance(mChatHandler.getReceiverId()));
        }

        /**
         * Get a fragment.
         * @param position Position of the fragment.
         * @return Fragment from this.mFragment
         */
        @Override
        public Fragment getItem(int position) {
            return this.mFragments.get(position);
        }

        /**
         * Get number of Fragments in adapter.
         * @return Number of Fragments
         */
        @Override
        public int getCount() {
            return this.mFragments.size();
        }
    }
}
