package com.mad.gamerfriends.model;

import java.util.ArrayList;

/**
 * Friend List Model
 * @author Wirawan Tjo
 * @version 1.0
 */
public class FriendList {
    private ArrayList<User> mUsers;

    public FriendList() {
        this.mUsers = new ArrayList<>();
    }

    /**
     * Deep Copy another FriendList Object
     * @param friendList FriendList Object
     */
    public FriendList(FriendList friendList) {
        this();

        for (User user : friendList.getUsers()) {
            this.mUsers.add(user);
        }
    }

    /**
     * Add user
     * @param user User Object
     */
    public void add(User user) {
        this.mUsers.add(user);
    }

    /**
     * Get user by position in Friend List
     * @param position Position Number
     * @return User Object
     */
    public User get(int position) {
        return this.mUsers.get(position);
    }

    /**
     * Get size of Friend List
     * @return Size of Friend List
     */
    public int size() {
        return this.mUsers.size();
    }

    /**
     * Clear the Friend List (Remove all users in Friend List)
     */
    public void clear() {
        this.mUsers.clear();
    }

    /**
     * Get a user by Steam ID. Loop through each user until the Steam ID match.
     * @param steamId Steam ID of friend
     * @return User Object
     */
    public User getUser(String steamId) {
        for (User user: this.mUsers) {
            if (user.getSteamId().equals(steamId)) {
                return user;
            }
        }
        return null;
    }

    public ArrayList<User> getUsers() {
        return this.mUsers;
    }

    public void setUsers(ArrayList<User> users) {
        this.mUsers = users;
    }
}
