package com.mad.gamerfriends.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by whanata on 24/9/17.
 */

public class GameAchievementResponse {
    @SerializedName("name")
    private String mId;
    @SerializedName("displayName")
    private String mName;
    @SerializedName("defaultvalue")
    private String mDefaultValue;
    @SerializedName("hidden")
    private int mHidden;
    @SerializedName("description")
    private String mDescription;
    @SerializedName("icon")
    private String mIcon;
    @SerializedName("icongray")
    private String mIconGray;

    public GameAchievementResponse(
            String mId,
            String mDefaultValue,
            String mName,
            int mHidden,
            String mDescription,
            String mIcon,
            String mIconGray) {
        this.mId = mId;
        this.mDefaultValue = mDefaultValue;
        this.mName = mName;
        this.mHidden = mHidden;
        this.mDescription = mDescription;
        this.mIcon = mIcon;
        this.mIconGray = mIconGray;
    }

    public String getId() {
        return this.mId.replaceAll("\\.|\\#|\\$|\\[|\\]", "_");
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getDefaultValue() {
        return this.mDefaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.mDefaultValue = defaultValue;
    }

    public String getName() {
        return this.mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public int getHidden() {
        return this.mHidden;
    }

    public void setHidden(int hidden) {
        this.mHidden = hidden;
    }

    public String getDescription() {
        return this.mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

    public String getIcon() {
        return this.mIcon;
    }

    public void setIcon(String icon) {
        this.mIcon = icon;
    }

    public String getIconGray() {
        return this.mIconGray;
    }

    public void setIconGray(String iconGray) {
        this.mIconGray = iconGray;
    }
}
