package com.mad.gamerfriends.model;

import com.google.firebase.database.Exclude;
import com.mad.gamerfriends.response.UserResponse;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * User Model
 * @author Wirawan Tjo
 * @version 1.0
 */
public class User {
    /**
     * A comparator constant for ordering username ascending.
     * Make sure the name is lowercase so there won't be issues with compare to between uppercase
     * and lowercase.
     */
    public static Comparator<User> COMPARE_BY_NAME_ASC = new Comparator<User>() {
        @Override
        public int compare(User user1, User user2) {
            return user1.getUsername().toLowerCase().compareTo(user2.getUsername().toLowerCase());
        }
    };

    private String mSteamId;
    private String mUsername;
    private String mRealName;
    private String mProfileUrl;
    // Medium Avatar
    private String mAvatarUrl;
    private String mState;
    private String mCountry;
    private FriendList mFriendList;
    private List<ChatThread> mChatThreads;
    private List<Game> mGames;

    public User() {
        this.mFriendList = new FriendList();
        this.mChatThreads = new ArrayList<>();
        this.mGames = new ArrayList<>();
    }

    public User(String steamId) {
        this();
        this.mSteamId = steamId;
        this.mUsername = "";
    }

    /**
     * Deep copy of another user object.
     * @param user User Object
     */
    public User(User user) {
        this.setSteamId(user.getSteamId());
        this.setUsername(user.getUsername());
        this.setRealName(user.getRealName());
        this.setProfileUrl(user.getProfileUrl());
        this.setAvatarUrl(user.getAvatarUrl());
        this.setState(user.getState());
        this.setCountry(user.getCountry());
        this.setFriendList(user.getFriendList());
        this.setChatThreads(user.getChatThreads());
    }

    /**
     * Copy information from UserResponse to this user.
     * @param userResponse UserResponse
     */
    public User(UserResponse userResponse) {
        this.setSteamId(userResponse.getSteamId());
        this.setUsername(userResponse.getUserName());
        this.setRealName(userResponse.getRealName());
        this.setProfileUrl(userResponse.getProfileUrl());
        this.setAvatarUrl(userResponse.getAvatarMedium());
        this.setState(userResponse.getStateCode());
        this.setCountry(userResponse.getCountryCode());
    }

    @Exclude
    public List<Game> getGames() {
        if (this.mGames == null) {
            this.mGames = new ArrayList<>();
        }
        return this.mGames;
    }

    public void setGames(List<Game> games) {
        this.mGames = new ArrayList<>();

        for (Game game : games) {
            this.mGames.add(game);
        }
    }

    public void addGame(Game game) {
        if (this.mGames == null) {
            this.mGames = new ArrayList<>();
        }
        this.mGames.add(game);
    }

    @Exclude
    public List<ChatThread> getChatThreads() {
        if (this.mChatThreads == null) {
            this.mChatThreads = new ArrayList<>();
        }
        return this.mChatThreads;
    }

    public void setChatThreads(List<ChatThread> chatThreads) {
        this.mChatThreads = new ArrayList<>();

        for (ChatThread chatThread : chatThreads) {
            this.mChatThreads.add(chatThread);
        }
    }

    @Exclude
    public FriendList getFriendList() {
        if (this.mFriendList == null) {
            this.mFriendList = new FriendList();
        }
        return this.mFriendList;
    }

    public void setFriendList(FriendList friendList) {
        this.mFriendList = new FriendList(friendList);
    }

    public User getUserFromFriendList(String steamId) {
        return this.mFriendList.getUser(steamId);
    }

    @Exclude
    public void addFriend(User user) {
        this.mFriendList.add(user);
    }

    public void clearFriendList() {
        this.mFriendList.clear();
    }

    public String getSteamId() {
        return this.mSteamId;
    }

    public void setSteamId(String steamId) {
        this.mSteamId = steamId;
    }

    public String getUsername() {
        return this.mUsername;
    }

    public void setUsername(String username) {
        this.mUsername = username;
    }

    public String getRealName() {
        return this.mRealName;
    }

    public void setRealName(String realName) {
        this.mRealName = realName;
    }

    public String getProfileUrl() {
        return this.mProfileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.mProfileUrl = profileUrl;
    }

    public String getCountry() {
        return this.mCountry;
    }

    public void setCountry(String country) {
        this.mCountry = country;
    }

    public String getState() {
        return this.mState;
    }

    public void setState(String state) {
        this.mState = state;
    }

    public String getAvatarUrl() {
        return this.mAvatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.mAvatarUrl = avatarUrl;
    }
}
