package com.mad.gamerfriends.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by whanata on 24/9/17.
 */

public class UserGameStatResponse {
    @SerializedName("playerstats")
    private PlayerStat playerStat;

    public UserGameStatResponse() {
        this.playerStat = new PlayerStat();
    }

    public PlayerStat getPlayerStat() {
        return this.playerStat;
    }

    public String getSteamId() {
        return this.playerStat.getSteamId();
    }

    public String getGameName() {
        return this.playerStat.getGameName();
    }

    public List<UserAchievementResponse> getAchievements() {
        return this.playerStat.getAchievements();
    }

    public List<UserStatResponse> getStats() {
        return this.playerStat.getStats();
    }

    public void setPlayerStat(PlayerStat playerStat) {
        this.playerStat = playerStat;
    }

    public class PlayerStat {
        @SerializedName("steamID")
        private String mSteamId;
        @SerializedName("gameName")
        private String mGameName;
        @SerializedName("achievements")
        private List<UserAchievementResponse> mAchievements;
        @SerializedName("stats")
        private List<UserStatResponse> mStats;

        public PlayerStat() {
            this.mAchievements = new ArrayList<>();
            this.mStats = new ArrayList<>();
        }

        public PlayerStat(
                String mSteamId,
                String mGameName,
                List<UserAchievementResponse> mAchievements,
                List<UserStatResponse> mStats) {
            this.mSteamId = mSteamId;
            this.mGameName = mGameName;
            this.mAchievements = mAchievements;
            this.mStats = mStats;
        }

        public String getSteamId() {
            return this.mSteamId;
        }

        public void setSteamId(String steamId) {
            this.mSteamId = steamId;
        }

        public String getGameName() {
            return this.mGameName;
        }

        public void setGameName(String gameName) {
            this.mGameName = gameName;
        }

        public List<UserAchievementResponse> getAchievements() {
            if (this.mAchievements == null) {
                this.mAchievements = new ArrayList<>();
            }

            return this.mAchievements;
        }

        public void setAchievements(List<UserAchievementResponse> achievements) {
            this.mAchievements = achievements;
        }

        public List<UserStatResponse> getStats() {
            if (this.mStats == null) {
                this.mStats = new ArrayList<>();
            }
            return this.mStats;
        }

        public void setStats(List<UserStatResponse> stats) {
            this.mStats = stats;
        }
    }
}
