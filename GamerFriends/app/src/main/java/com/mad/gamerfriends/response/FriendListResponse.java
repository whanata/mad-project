package com.mad.gamerfriends.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by whanata on 2/9/17.
 */

public class FriendListResponse {
    @SerializedName("friendslist")
    private FriendList mFriendList;

    public FriendListResponse(FriendList friendList) {
        this.mFriendList = friendList;
    }

    public List<FriendResponse> getFriendsList() {
        return this.mFriendList.getFriendList();
    }

    public void setFriendsList(FriendList friendList) {
        this.mFriendList = friendList;
    }

    private class FriendList {
        @SerializedName("friends")
        private List<FriendResponse> mFriendList;

        public FriendList(List<FriendResponse> friendList) {
            this.mFriendList = friendList;
        }

        public List<FriendResponse> getFriendList() {
            return this.mFriendList;
        }

        public void setFriendList(List<FriendResponse> friendList) {
            this.mFriendList = friendList;
        }
    }
}
