package com.mad.gamerfriends.model;

/**
 * Single message in a chat thread.
 * @author Wirawan Tjo
 * @version 1.0
 */
public class ChatMessage {
    private String mTimestamp;
    private String mSteamId;
    private String mText;

    public ChatMessage() {
        this(null, null, null);
    }

    public ChatMessage(String timestamp, String steamId, String text) {
        this.mTimestamp = timestamp;
        this.mSteamId = steamId;
        this.mText = text;
    }

    public String getTimestamp() {
        return this.mTimestamp;
    }

    public void setTimestamp(String timestamp) {
        this.mTimestamp = timestamp;
    }

    public String getSteamId() {
        return this.mSteamId;
    }

    public void setSteamId(String steamId) {
        this.mSteamId = steamId;
    }

    public String getText() {
        return this.mText;
    }

    public void setText(String text) {
        this.mText = text;
    }
}
