package com.mad.gamerfriends.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.mad.gamerfriends.adapter.GameListRecyclerAdapter;
import com.mad.gamerfriends.R;
import com.mad.gamerfriends.handler.GameListHandler;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;

/**
 * Fragment for the Owned Games the user have. This is in the MainActivity.
 * @author Wirawan Tjo
 * @version 1.0
 */
public class GameListFragment extends Fragment {
    public static final String LOG_TAG = "GameListFragment";
    public static final String STEAM_ID_KEY = "Steam ID";

    @BindView(R.id.fragment_main_game_list_RecyclerView)
    protected RecyclerView mRecyclerView;
    @BindView(R.id.fragment_main_game_list_ProgressBar)
    protected ProgressBar mProgressBar;

    private CompositeDisposable mCompositeDisposable;
    private Unbinder mUnbinder;
    private GameListHandler mGameListHandler;
    private GameListRecyclerAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    /**
     * Create new instance of this fragment.
     * @return GameListFragment instance
     */
    public static GameListFragment newInstance() {
        return new GameListFragment();
    }

    /**
     * Initialise variables that doesn't require the view.
     * @param savedInstanceState Saved information from previous state
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mCompositeDisposable = new CompositeDisposable();
        this.mGameListHandler = new GameListHandler();
    }

    /**
     * Initialise the variables from the fragment that requires view. Includes initialising
     * Butterknife and setting RecyclerView. Also getGameList from the Firebase DB.
     * @param inflater Used to inflate the fragment's view on the activity
     * @param container The container where the fragment's view is inflated.
     * @param savedInstanceState Saved information from previous state
     * @return The view for this fragment.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_game_list, container, false);
        this.mUnbinder = ButterKnife.bind(this, view);

        this.mCompositeDisposable.add(
                this.mGameListHandler.getGameList(this.getGameObserver()));

        this.mAdapter = new GameListRecyclerAdapter(getActivity());
        this.mLayoutManager = new LinearLayoutManager(getActivity());
        this.mRecyclerView.setLayoutManager(this.mLayoutManager);
        this.mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        this.mRecyclerView.setAdapter(this.mAdapter);

        return view;
    }

    /**
     * Unbind the butteknife instance and clear all the observers.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mCompositeDisposable.clear();
        this.mUnbinder.unbind();
    }

    /**
     * Sort Game List by name, remove ProgressBar and show RecyclerView
     */
    private void updateGameList() {
        this.mGameListHandler.sortGameList(GameListHandler.SORT_BY_NAME);
        this.mProgressBar.setVisibility(View.GONE);
        this.mRecyclerView.setVisibility(View.VISIBLE);
        this.mAdapter.notifyDataSetChanged();
    }

    /**
     * Observer for getting each game from the Observable.
     * @return Observer for getting a game
     */
    protected DisposableObserver<String> getGameObserver() {
        return new DisposableObserver<String>() {

            @Override
            public void onNext(@NonNull String text) {
                Log.d(GameListFragment.LOG_TAG, "onNext: getGame " + text);
            }

            /**
             * Update the GameList if there are any errors.
             * @param e Exception
             */
            @Override
            public void onError(@NonNull Throwable e) {
                Log.e(GameListFragment.LOG_TAG, "onError: getGame", e);
                updateGameList();
            }

            /**
             * Update the GameList
             */
            @Override
            public void onComplete() {
                Log.d(GameListFragment.LOG_TAG, "onComplete: getGame");
                updateGameList();
            }
        };
    }
}
