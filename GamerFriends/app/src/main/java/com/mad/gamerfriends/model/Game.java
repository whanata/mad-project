package com.mad.gamerfriends.model;

import com.mad.gamerfriends.response.GameAchievementResponse;
import com.mad.gamerfriends.response.GameSchemaResponse;
import com.mad.gamerfriends.response.GameStatResponse;
import com.mad.gamerfriends.response.OwnedGameResponse;
import com.mad.gamerfriends.response.UserAchievementResponse;
import com.mad.gamerfriends.response.UserGameStatResponse;
import com.mad.gamerfriends.response.UserStatResponse;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Game Model for a specific user.
 * @author Wirawan Tjo
 * @version 1.0
 */
public class Game {
    /**
     * A comparator constant for ordering name ascending.
     */
    public static Comparator<Game> COMPARE_BY_NAME_ASC = new Comparator<Game>() {
        @Override
        public int compare(Game game1, Game game2) {
            return game1.getName().toLowerCase().compareTo(game2.getName().toLowerCase());
        }
    };

    public static final String HASH_URL =
            "http://media.steampowered.com/steamcommunity/public/images/apps/%s/%s.jpg";

    private String mAppId;
    private String mName;
    private String mLastUpdated;
    private String mIconUrl;
    private String mLogoUrl;
    private int mTwoWeekPlaytime;
    private int mTotalPlaytime;
    private List<GameAchievement> mAchievements;
    private List<GameStat> mStats;

    public Game() {
        this.mAppId = "";
        this.mName = "";
        this.mIconUrl = "";
        this.mLogoUrl = "";
        this.mTwoWeekPlaytime = 0;
        this.mTotalPlaytime = 0;
        this.mLastUpdated = Long.toString(System.currentTimeMillis());
        this.mAchievements = new ArrayList<>();
        this.mStats = new ArrayList<>();
    }

    public Game(String appId) {
        this();
        this.mAppId = appId;
    }

    /**
     * Adding information from OwnedGameResponse to Game Object.
     * Getting basic game information.
     * @param response OwnedGameResponse
     */
    public void addOwnedGameResponse(OwnedGameResponse response) {
        this.mAppId = response.getAppId();
        this.mName = response.getName();
        this.mTwoWeekPlaytime = response.getTwoWeeksPlaytime();
        this.mTotalPlaytime = response.getTotalPlaytime();
        this.setHashIcon(response.getIconHash());
        this.setHashLogo(response.getLogoHash());
    }

    /**
     * Adding information from GameSchemaResponse and UserGameStatResponse to Game Object.
     * Adding stats and achievement information.
     * @param gameResponse
     * @param userResponse
     */
    public void addGameAchievementStats(GameSchemaResponse gameResponse,
                                      UserGameStatResponse userResponse) {
        this.mAchievements.clear();
        this.mStats.clear();

        for (GameAchievementResponse achievementResponse :
                gameResponse.getPossibleStats().getAchievements()) {
            this.mAchievements.add(new GameAchievement(achievementResponse));
        }

        for (UserAchievementResponse achievementResponse :
                userResponse.getAchievements()) {
            this.getAchievement(achievementResponse.getId())
                    .addUserAchievementResponse(achievementResponse);
        }

        for (GameStatResponse statResponse : gameResponse.getPossibleStats().getStats()) {
            this.mStats.add(new GameStat(statResponse));
        }

        for (UserStatResponse statResponse :
                userResponse.getStats()) {
            this.getStat(statResponse.getId())
                    .addUserStatResponse(statResponse);
        }
    }

    public GameAchievement getAchievement(String achievementId) {
        for (GameAchievement achievement : this.mAchievements) {
            if (achievement.getId().equals(achievementId)) {
                return achievement;
            }
        }
        return null;
    }

    public GameStat getStat(String statId) {
        for (GameStat stat : this.mStats) {
            if (stat.getId().equals(statId)) {
                return stat;
            }
        }
        return null;
    }

    public String getAppId() {
        return this.mAppId;
    }

    public void setAppId(String appId) {
        this.mAppId = appId;
    }

    public String getName() {
        return this.mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getIconUrl() {
        return this.mIconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.mIconUrl = iconUrl;
    }

    public void setHashIcon(String hash) {
        this.mIconUrl = this.hashToUrl(hash);
    }

    public String getLogoUrl() {
        return this.mLogoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.mLogoUrl = logoUrl;
    }

    public void setHashLogo(String hash) {
        this.mLogoUrl = this.hashToUrl(hash);
    }

    private String hashToUrl(String hash) {
        return String.format(Game.HASH_URL, this.mAppId, hash);
    }

    public String getLastUpdated() {
        return this.mLastUpdated;
    }

    public void setLastUpdated(String lastUpdated) {
        this.mLastUpdated = lastUpdated;
    }

    public List<GameAchievement> getAchievements() {
        return this.mAchievements;
    }

    public void setAchievements(List<GameAchievement> achievements) {
        this.mAchievements = new ArrayList<>();

        for (GameAchievement achivement : achievements) {
            this.mAchievements.add(achivement);
        }
    }

    public List<GameStat> getStats() {
        return this.mStats;
    }

    public void setStats(List<GameStat> stats) {
        this.mStats = new ArrayList<>();

        for (GameStat stat : stats) {
            this.mStats.add(stat);
        }
    }

    public int getTwoWeekPlaytime() {
        return this.mTwoWeekPlaytime;
    }

    public void setTwoWeekPlaytime(int twoWeekPlaytime) {
        this.mTwoWeekPlaytime = twoWeekPlaytime;
    }

    public int getTotalPlaytime() {
        return this.mTotalPlaytime;
    }

    public void setTotalPlaytime(int totalPlaytime) {
        this.mTotalPlaytime = totalPlaytime;
    }
}
