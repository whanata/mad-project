package com.mad.gamerfriends.fragment.chat;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.mad.gamerfriends.adapter.GameListRecyclerAdapter;
import com.mad.gamerfriends.R;
import com.mad.gamerfriends.handler.GameListHandler;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;

/**
 * Fragment for the game list of a user from chat.
 * @author Wirawan Tjo
 * @version 1.0
 */
public class ChatGameListFragment extends Fragment {
    public static final String LOG_TAG = "ChatGameListFragment";
    public static final String RECEIVER_ID_KEY = "Receiver Steam ID";

    @BindView(R.id.fragment_chat_game_list_RecyclerView)
    protected RecyclerView mRecyclerView;
    @BindView(R.id.fragment_chat_game_list_ProgressBar)
    protected ProgressBar mProgressBar;

    private CompositeDisposable mCompositeDisposable;
    private Unbinder mUnbinder;
    private String mReceiverId;
    private GameListHandler mGameListHandler;
    private GameListRecyclerAdapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    /**
     * Create new instance of this fragment.
     * @param receiverId The chat recipient steam ID
     * @return ChatGameListFragment instance
     */
    public static ChatGameListFragment newInstance(String receiverId) {
        ChatGameListFragment ChatGameListFragment = new ChatGameListFragment();
        Bundle args = new Bundle();
        args.putString(ChatGameListFragment.RECEIVER_ID_KEY, receiverId);
        ChatGameListFragment.setArguments(args);
        return ChatGameListFragment;
    }

    /**
     * Instantiate variables that doesn't require a view.
     * @param savedInstanceState Saved information from previous state
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.mReceiverId = getArguments().getString(ChatGameListFragment.RECEIVER_ID_KEY);
        this.mCompositeDisposable = new CompositeDisposable();
        this.mGameListHandler = new GameListHandler(this.mReceiverId);
    }

    /**
     * Instantiate variables that require a view. This includes butterknife instance and
     * GameListHandler.
     * @param inflater Used to inflate the fragment's view on the activity
     * @param container The container where the fragment's view is inflated.
     * @param savedInstanceState Saved information from previous state
     * @return The view for this fragment.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_chat_game_list, container, false);
        ButterKnife.setDebug(true);
        this.mUnbinder = ButterKnife.bind(this, view);

        this.mCompositeDisposable.add(
                mGameListHandler.getGameList(this.getGameObserver()));

        this.mAdapter = new GameListRecyclerAdapter(
                getActivity(), this.mGameListHandler.getUserHandler());
        this.mLayoutManager = new LinearLayoutManager(getActivity());
        this.mRecyclerView.setLayoutManager(this.mLayoutManager);
        this.mRecyclerView.setItemAnimator(new DefaultItemAnimator());
        this.mRecyclerView.setAdapter(this.mAdapter);

        return view;
    }

    /**
     * Clear any observers and unbind the butterknife instance.
     */
    @Override
    public void onDestroy() {
        super.onDestroy();
        this.mCompositeDisposable.clear();
        this.mUnbinder.unbind();
    }

    /**
     * Show the GameList RecyclerView and remove the ProgressBar. Also sort the games by names.
     */
    private void updateGameList() {
        this.mGameListHandler.sortGameList(GameListHandler.SORT_BY_NAME);
        this.mProgressBar.setVisibility(View.GONE);
        this.mRecyclerView.setVisibility(View.VISIBLE);
        this.mAdapter.notifyDataSetChanged();
    }

    /**
     * Observer for getting each game from the Observable.
     * @return Observer for getting a game
     */
    protected DisposableObserver<String> getGameObserver() {
        return new DisposableObserver<String>() {

            @Override
            public void onNext(@NonNull String text) {
                Log.d(ChatGameListFragment.LOG_TAG, "onNext: getGame " + text);
            }

            /**
             * Update the GameList if there are any errors.
             * @param e Exception
             */
            @Override
            public void onError(@NonNull Throwable e) {
                Log.e(ChatGameListFragment.LOG_TAG, "onError: getGame", e);
                updateGameList();
            }

            /**
             * Update the GameList
             */
            @Override
            public void onComplete() {
                Log.d(ChatGameListFragment.LOG_TAG, "onComplete: getGame");
                updateGameList();
            }
        };
    }
}
