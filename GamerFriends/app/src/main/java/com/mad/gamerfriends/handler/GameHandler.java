package com.mad.gamerfriends.handler;

import android.util.Log;

import com.mad.gamerfriends.api.SteamWebApi;
import com.mad.gamerfriends.model.Game;
import com.mad.gamerfriends.model.GameAchievement;
import com.mad.gamerfriends.model.GameStat;
import com.mad.gamerfriends.response.GameSchemaResponse;
import com.mad.gamerfriends.response.OwnedGameListResponse;
import com.mad.gamerfriends.response.OwnedGameResponse;
import com.mad.gamerfriends.response.UserGameStatResponse;

import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.BiFunction;
import io.reactivex.functions.Function;
import io.reactivex.functions.Predicate;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Handles all the activity associated with a specific game played by a specific user.
 * @author Wirawan Tjo
 * @version 1.0
 */
public class GameHandler {
    public static final String SORT_BY_GAME_STAT_NAME = "Sort By Stat Name";
    public static final String SORT_BY_GAME_ACHIEVEMENT_NAME = "Sort By Achievement Name";

    private String mSteamId;
    private SteamWebApi mApi;
    private Game mGame;

    /**
     * Constructor to create GameHandler, initialise variables such as the Steam Web API Object.
     * @param steamId Steam ID of user
     * @param appId App ID of game from Steam
     */
    public GameHandler(String steamId, String appId) {
        this.mApi = SteamWebApi.getInstance();
        this.mSteamId = steamId;
        this.mGame = new Game(appId);
    }

    /**
     * Get Game
     * @return Game Object
     */
    public Game getGame() {
        return this.mGame;
    }

    /**
     * Get Game Name
     * @return Name of Game
     */
    public String getGameName() {
        return this.mGame.getName();
    }

    /**
     * Get list of stats for that game for a user
     * @return List of stats
     */
    public List<GameStat> getStats() {
        return this.mGame.getStats();
    }

    /**
     * Get list of achievements for that game for a user
     * @return List of achievements
     */
    public List<GameAchievement> getAchievements() {
        return this.mGame.getAchievements();
    }

    /**
     * Get Game information for a specific user using the Steam API. Get OwnedGameList (List of
     * App ID of games that the user owns). Filter for each games to get the game with the same
     * app ID. For that game, do 2 API calls to get both the Game Schema
     * and Game information for that specific user. Do this in multiple threads.
     * @param observer Observer for this Disposable
     * @return Disposable for getting a game
     */
    public Disposable getGame(DisposableObserver<String> observer) {
        final String steamId = this.mSteamId;
        final String appId = this.mGame.getAppId();
        return this.mApi.getOwnedGames(steamId)
                .flatMapIterable(new Function<OwnedGameListResponse, Iterable<OwnedGameResponse>>() {

                    @Override
                    public Iterable<OwnedGameResponse> apply(
                            @NonNull OwnedGameListResponse ownedGameListResponse) throws Exception {
                        return ownedGameListResponse.getGameList();
                    }
                })
                .filter(new Predicate<OwnedGameResponse>() {

                    @Override
                    public boolean test(@NonNull OwnedGameResponse ownedGameResponse) throws Exception {
                        return ownedGameResponse.getAppId().equals(appId);
                    }
                })
                .flatMap(new Function<OwnedGameResponse, ObservableSource<String>>() {

                    @Override
                    public ObservableSource<String> apply(
                            final @NonNull OwnedGameResponse ownedGameResponse) throws Exception {
                        return Observable.zip(
                                mApi.getGameSchema(appId),
                                mApi.getUserGameStats(steamId, appId),
                                new BiFunction<GameSchemaResponse, UserGameStatResponse, String>() {

                                    @Override
                                    public String apply(
                                            @NonNull GameSchemaResponse gameSchemaResponse,
                                            @NonNull UserGameStatResponse userGameStatResponse)
                                            throws Exception {
                                        initialiseGame(ownedGameResponse,
                                                gameSchemaResponse,
                                                userGameStatResponse);

                                        return String.format("%s: %s", steamId, appId);
                                    }
                                })
                                .subscribeOn(Schedulers.io())
                                .onErrorResumeNext(
                                        new Function<Throwable, ObservableSource<String>>() {

                                            @Override
                                            public ObservableSource<String> apply(
                                                    @NonNull Throwable e) throws Exception {
                                                initialiseGame(ownedGameResponse);
                                                Log.e(GameListHandler.LOG_TAG, "onError: getGames", e);
                                                return Observable.just(
                                                        String.format("Error: %s: %s", steamId, appId));
                                            }
                                        });
                    }
                })
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(observer);
    }

    /**
     * Add information from OwnedGameResponse (Steam API response) to the game object.
     * @param ownedGameResponse Response from OwnedGameList
     */
    private void initialiseGame(OwnedGameResponse ownedGameResponse) {
        this.mGame.addOwnedGameResponse(ownedGameResponse);
    }

    /**
     * Add information from OwnedGameResponse, GameSchemaResponse, UserGameStatResponse to the game
     * object.
     * @param ownedGameResponse Response from OwnedGameList
     * @param gameSchemaResponse Response from GameSchema
     * @param userGameStatResponse Response from UserGameStats
     */
    private void initialiseGame(
            OwnedGameResponse ownedGameResponse,
            GameSchemaResponse gameSchemaResponse,
            UserGameStatResponse userGameStatResponse) {
        this.mGame.addOwnedGameResponse(ownedGameResponse);
        this.mGame.addGameAchievementStats(gameSchemaResponse, userGameStatResponse);
    }

    /**
     * Choose to sort either stats by name or achievement by name.
     * @param by String for which list to sort.
     */
    public void sort(String by) {
        switch(by) {
            case GameHandler.SORT_BY_GAME_STAT_NAME:
                List<GameStat> stats = this.mGame.getStats();
                Collections.sort(stats, GameStat.COMPARE_BY_NAME_ASC);
                break;
            case GameHandler.SORT_BY_GAME_ACHIEVEMENT_NAME:
                List<GameAchievement> achievements = this.mGame.getAchievements();
                Collections.sort(achievements, GameAchievement.COMPARE_BY_NAME_ASC);
                break;
            default:
                break;
        }
    }
}
