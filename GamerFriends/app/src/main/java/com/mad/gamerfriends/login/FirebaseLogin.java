package com.mad.gamerfriends.login;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.ProviderQueryResult;

import java.util.List;

/**
 * Handle login on Firebase. This is a singleton.
 * @author Wirawan Tjo
 * @version 1.0
 */
public class FirebaseLogin {
    public static final String LOG_TAG = "FirebaseLogin";

    private static final String PASSWORD = "whanata";
    private static FirebaseLogin sInstance = null;

    private FirebaseAuth mAuth;

    /**
     * Get instance of FirebaseLogin, if it exist return an existing instance, if not, return a
     * new FirebaseLogin instance.
     * @return FirebaseLogin instance
     */
    public static synchronized FirebaseLogin getInstance() {
        if (FirebaseLogin.sInstance == null) {
            FirebaseLogin.sInstance = new FirebaseLogin();
        }

        return FirebaseLogin.sInstance;
    }

    /**
     * Private Constructor (Singleton) - Get FirebaseAuth instance.
     */
    private FirebaseLogin() {
        this.mAuth = FirebaseAuth.getInstance();
    }

    /**
     * This basically sign up a new user using the Steam ID if they are not in the Firebase Auth
     * DB or Login an existing user that has use the app.
     * @param steamId Steam ID for login
     */
    public void authorizeUser(String steamId) {
        final String steamEmail = String.format("%s@steam.com", steamId);
        // Check if email exist
        OnCompleteListener onCompleteListener = new OnCompleteListener<ProviderQueryResult>() {
            @Override
            public void onComplete(@NonNull Task<ProviderQueryResult> task) {
                if (task.isSuccessful()) {
                    // getProviders() will return size 1. if email ID is available.
                    List<String> providers = task.getResult().getProviders();
                    if (providers != null) {
                        if (providers.size() > 0) {
                            signInExistingUser(steamEmail);
                        } else {
                            signUpNewUser(steamEmail);
                        }
                    }
                }
            }
        };

        this.mAuth.fetchProvidersForEmail(steamEmail).addOnCompleteListener(onCompleteListener);
    }

    /**
     * Sign up new user.
     * @param steamEmail Email to sign up on
     */
    private void signUpNewUser(String steamEmail) {
        OnCompleteListener<AuthResult> onCompleteListener = new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                Log.d(FirebaseLogin.LOG_TAG, "signUpNewUser:onComplete:" + task.isSuccessful());

            }
        };

        this.mAuth.createUserWithEmailAndPassword(steamEmail, FirebaseLogin.PASSWORD)
                .addOnCompleteListener(onCompleteListener);
    }

    /**
     * Login an existing user
     * @param steamEmail Email to login
     */
    private void signInExistingUser(String steamEmail) {
        OnCompleteListener<AuthResult> onCompleteListener = new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                Log.d(FirebaseLogin.LOG_TAG, "signInExistingUser:onComplete:" + task.isSuccessful());
            }
        };

        this.mAuth.signInWithEmailAndPassword(steamEmail, FirebaseLogin.PASSWORD)
                .addOnCompleteListener(onCompleteListener);
    }
}
