package com.mad.gamerfriends.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by whanata on 24/9/17.
 */

public class UserAchievementResponse {
    @SerializedName("name")
    private String mId;
    @SerializedName("achieved")
    private String mAchieved;

    public UserAchievementResponse(String mId, String mAchieved) {
        this.mId = mId;
        this.mAchieved = mAchieved;
    }

    public String getId() {
        return this.mId.replaceAll("\\.|\\#|\\$|\\[|\\]", "_");
    }

    public void setId(String name) {
        this.mId = name;
    }

    public String getAchieved() {
        return this.mAchieved;
    }

    public void setAchieved(String achieved) {
        this.mAchieved = achieved;
    }
}
