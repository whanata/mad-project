package com.mad.gamerfriends.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;
import com.mad.gamerfriends.R;
import com.mad.gamerfriends.activity.ChatActivity;
import com.mad.gamerfriends.handler.CurrentUserHandler;
import com.mad.gamerfriends.model.FriendList;
import com.mad.gamerfriends.model.User;
import com.makeramen.roundedimageview.RoundedImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Adapter Class for the FriendListFragment RecyclerView. Shows the Friend List for current user.
 * @author  Wirawan Tjo
 * @version 1.0
 */
public class FriendListRecyclerAdapter extends RecyclerView.Adapter<FriendListRecyclerAdapter.ViewHolder> {
    public static final String LOG_TAG = "FriendRecyclerAdapter";

    private Context mContext;
    private CurrentUserHandler mCurrentUserHandler;

    /**
     * Instantiate FriendListRecyclerAdapter
     * @param context Context where this adapter is instantiated.
     */
    public FriendListRecyclerAdapter(Context context) {
        this.mContext = context;
        this.mCurrentUserHandler = CurrentUserHandler.getInstance();
    }

    /**
     * Inner class - Represents each item in the RecyclerView. Each instance of this class is one
     * item in the RecyclerView.
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        public static final String RECEIVER_ID_KEY = "Receiver ID";

        @BindView(R.id.item_friend_body_ConstraintLayout)
        protected ConstraintLayout mFriendItemBodyConstraintLayout;
        @BindView(R.id.item_friend_profile_image_RoundedImageView)
        protected RoundedImageView mFriendItemProfileImageView;
        @BindView(R.id.item_friend_username_TextView)
        protected TextView mFriendItemUsernameTextView;

        private String mSteamId;

        /**
         * The constructor for this class.
         * Gets all the views (widgets and layout) for this each row of the list of friends.
         * @param itemView The Item View for this ViewHolder
         */
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            this.mSteamId = "";
        }

        /**
         * Sets the User Information to the item layout.
         * @param user User for this ViewHolder
         */
        public void setUserInfo(User user) {
            this.mSteamId = user.getSteamId();
            Ion.with(this.mFriendItemProfileImageView)
                    .placeholder(R.drawable.ic_placeholder_profile)
                    .error(R.drawable.ic_placeholder_profile)
                    .animateIn(R.anim.fade_in)
                    .load(user.getAvatarUrl());
            String name;
            if (user.getRealName() != null) {
                name = String.format("%s (%s)", user.getUsername(), user.getRealName());
            } else {
                name = String.format("%s", user.getUsername());
            }
            this.mFriendItemUsernameTextView.setText(name);
        }

        /**
         * On clicking the main body of the item, start ChatActivity for this user.
         */
        @OnClick(R.id.item_friend_body_ConstraintLayout)
        public void chatAction() {
            Intent chatIntent = new Intent(mContext, ChatActivity.class);
            chatIntent.putExtra(ViewHolder.RECEIVER_ID_KEY, this.mSteamId);
            mContext.startActivity(chatIntent);
        }
    }

    /**
     * Create a single ViewHolder.
     * @param parent The parent view for the adapter
     * @param viewType Type of view
     * @return new ViewHolder Object
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_friend, parent, false);
        return new ViewHolder(itemView);
    }

    /**
     * Bind ViewHolder to a User Object
     * @param holder The ViewHolder Object.
     * @param position Position of the ViewHolder in context of the RecylerView
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        User user = this.getFriendList().get(position);
        holder.setUserInfo(user);
    }

    /**
     * Count of the rows in the Adapter.
     * @return Count of rows in the Adapter.
     */
    @Override
    public int getItemCount() {
        return this.getFriendList().size();
    }

    /**
     * Get FriendList used by this adapter.
     * @return FriendList used for this adapter.
     */
    private FriendList getFriendList() {
        return this.mCurrentUserHandler.getFriendList();
    }
}
