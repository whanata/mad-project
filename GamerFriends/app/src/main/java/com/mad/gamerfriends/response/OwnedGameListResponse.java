package com.mad.gamerfriends.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by whanata on 23/9/17.
 */

public class OwnedGameListResponse {
    @SerializedName("response")
    private GameList mGameList;

    public OwnedGameListResponse(GameList mGameList) {
        this.mGameList = mGameList;
    }

    public class GameList {
        @SerializedName("games")
        private List<OwnedGameResponse> mGameList;
        @SerializedName("game_count")
        private int mGameCount;

        public GameList() {
            this.mGameList = new ArrayList<>();
        }

        public GameList(List<OwnedGameResponse> mGameList, int mGameCount) {
            this.mGameList = mGameList;
            this.mGameCount = mGameCount;
        }

        public List<OwnedGameResponse> getGameList() {
            return this.mGameList;
        }

        public void setGameList(List<OwnedGameResponse> gameList) {
            this.mGameList = gameList;
        }

        public int getGameCount() {
            return this.mGameCount;
        }

        public void setGameCount(int gameCount) {
            this.mGameCount = gameCount;
        }
    }

    public List<OwnedGameResponse> getGameList() {
        return this.mGameList.getGameList();
    }

    public int getGameCount() {
        return this.mGameList.getGameCount();
    }

    public void setGameList(GameList gameList) {
        this.mGameList = gameList;
    }
}
