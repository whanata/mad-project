package com.mad.gamerfriends.adapter;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.koushikdutta.ion.Ion;
import com.mad.gamerfriends.R;
import com.mad.gamerfriends.handler.GameHandler;
import com.mad.gamerfriends.model.GameAchievement;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Adapter Class for the GameAchievementFragment RecyclerView. Shows the Achievement List for a
 * specific game by a specific user
 * @author  Wirawan Tjo
 * @version 1.0
 */
public class GameAchievementRecyclerAdapter extends RecyclerView.Adapter<GameAchievementRecyclerAdapter.ViewHolder> {
    public static final String LOG_TAG = "GameAchievementRecyclerAdapter";

    private GameHandler mGameHandler;

    /**
     * Instantiates GameAchievementRecyclerAdapter
     * @param gameHandler GameHandler used in the activity for this adapter.
     */
    public GameAchievementRecyclerAdapter(GameHandler gameHandler) {
        this.mGameHandler = gameHandler;
    }

    /**
     * Inner class - Represents each item in the RecyclerView. Each instance of this class is one
     * item in the RecyclerView.
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_game_achievement_logo_ImageView)
        protected ImageView mAchievementLogoImageView;
        @BindView(R.id.item_game_achievement_name_TextView)
        protected TextView mAchievementNameTextView;
        @BindView(R.id.item_game_achievement_description_TextView)
        protected TextView mAchievementDescriptionTextView;
        @BindView(R.id.item_game_achievement_body_ConstraintLayout)
        protected ConstraintLayout mAchievementBodyConstraintLayout;

        /**
         * The constructor for this class.
         * Gets all the views (widgets and layout) for this each row of the list of achievements.
         * @param itemView The view of the item layout
         */
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        /**
         * Sets the Achievement Information to the item layout.
         * @param achievement Achievement for this item
         */
        public void setAchievement(GameAchievement achievement) {
            Ion.with(this.mAchievementLogoImageView)
                    .placeholder(R.drawable.ic_placeholder_profile)
                    .error(R.drawable.ic_placeholder_profile)
                    .animateIn(R.anim.fade_in)
                    .load(achievement.getIconUrl());

            String achievementName = "";
            // If name doesn't exist, use the ID instead for the name of the achievement
            if (!achievement.getName().trim().equals("")) {
                achievementName = achievement.getName();
            } else {
                achievementName = achievement.getId();
            }
            achievementName = achievementName.replace("_", " ").toLowerCase();

            // If achievement is done, colour it grey
            if (achievement.getAchieved()) {
                this.mAchievementBodyConstraintLayout
                        .setBackgroundResource(R.color.achievement_done);
            }

            this.mAchievementNameTextView.setText(achievementName);
            this.mAchievementDescriptionTextView.setText(achievement.getDescription());
        }
    }

    /**
     * Create a single ViewHolder.
     * @param parent The parent view for the adapter
     * @param viewType Type of view
     * @return new ViewHolder Object
     */
    @Override
    public GameAchievementRecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_game_achievement, parent, false);
        return new GameAchievementRecyclerAdapter.ViewHolder(itemView);
    }

    /**
     * Bind ViewHolder to a Achievement Object
     * @param holder The ViewHolder Object.
     * @param position Position of the ViewHolder in context of the RecylerView
     */
    @Override
    public void onBindViewHolder(GameAchievementRecyclerAdapter.ViewHolder holder, int position) {
        GameAchievement achievement = this.getGameAchievements().get(position);
        holder.setAchievement(achievement);
    }

    /**
     * Count of the rows in the RecyclerView.
     * @return Count of rows in the RecyclerView.
     */
    @Override
    public int getItemCount() {
        return this.getGameAchievements().size();
    }

    /**
     * Make mGameHandler = null to make sure it does not leak the activity to another class.
     * @param recyclerView
     */
    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        this.mGameHandler = null;
    }

    /**
     * Get GameAchievements used by this adapter.
     * @return GameAchievments used for this adapter.
     */
    private List<GameAchievement> getGameAchievements() {
        return this.mGameHandler.getAchievements();
    }
}
