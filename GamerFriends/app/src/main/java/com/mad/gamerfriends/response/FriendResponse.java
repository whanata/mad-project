package com.mad.gamerfriends.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by whanata on 2/9/17.
 */

public class FriendResponse {
    @SerializedName("steamid")
    private String mSteamId;
    @SerializedName("relationship")
    private String mRelationship;
    @SerializedName("friend_since")
    private String mFriendSince;

    public FriendResponse(String steamId, String relationship, String friendSince) {
        this.mSteamId = steamId;
        this.mRelationship = relationship;
        this.mFriendSince = friendSince;
    }

    public String getSteamId() {
        return this.mSteamId;
    }

    public void setSteamId(String steamId) {
        this.mSteamId = steamId;
    }

    public String getRelationship() {
        return this.mRelationship;
    }

    public void setRelationship(String relationship) {
        this.mRelationship = relationship;
    }

    public String getFriendSince() {
        return this.mFriendSince;
    }

    public void setFriendSince(String friendSince) {
        this.mFriendSince = friendSince;
    }
}
