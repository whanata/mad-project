package com.mad.gamerfriends.activity;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.gigamole.navigationtabstrip.NavigationTabStrip;
import com.mad.gamerfriends.adapter.GameListRecyclerAdapter;
import com.mad.gamerfriends.R;
import com.mad.gamerfriends.fragment.GameAchievementFragment;
import com.mad.gamerfriends.fragment.GameStatFragment;
import com.mad.gamerfriends.handler.GameHandler;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;

/**
 * The activity which shows a specific game's achievement and stats for a specific user. This uses
 * 2 fragments to show their achievement and stats for a specific game.
 * @author  Wirawan Tjo
 * @version 1.0
 */
public class GameActivity extends AppCompatActivity {
    public static final String LOG_TAG = "GameActivity";

    @BindView(R.id.activity_game_ProgressBar)
    protected ProgressBar mProgressBar;
    @BindView(R.id.activity_game_ViewPager)
    protected ViewPager mViewPager;
    @BindView(R.id.activity_game_NavigationTabStrip)
    protected NavigationTabStrip mNavigationTabStrip;
    @BindView(R.id.activity_game_name_TextView)
    protected TextView mGameNameTextView;

    private GameHandler mGameHandler;
    private Unbinder mUnbinder;
    private CompositeDisposable mCompositeDisposable;
    private GamePagerAdapter mGamePagerAdapter;

    /**
     * Instantiate GameActivity - run this during a new instantiation of GameActivity.
     * @param savedInstanceState Saved information from previous state
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game);
        this.mUnbinder = ButterKnife.bind(this);

        this.mGameHandler = new GameHandler(this.getSteamId(), this.getAppId());

        this.mCompositeDisposable = new CompositeDisposable();

        this.mCompositeDisposable.add(this.mGameHandler.getGame(this.getGameObserver()));

        this.mGamePagerAdapter = new GamePagerAdapter(getSupportFragmentManager());
        this.mViewPager.setAdapter(this.mGamePagerAdapter);
        this.mNavigationTabStrip.setViewPager(this.mViewPager, 0);
    }

    /**
     * Unbind Butterknife instance and clear all Observer to an Observable.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.mUnbinder.unbind();
        this.mCompositeDisposable.clear();
    }

    /**
     * Get the GameHandler Object from this.mGameHandler
     *
     * @return The GameHandler Object
     */
    public GameHandler getGameHandler() {
        return this.mGameHandler;
    }

    /**
     * Get Steam ID from the intent (The user's Steam ID for this activity).
     *
     * @return The Steam ID
     */
    protected String getSteamId() {
        Intent receiveIntent = getIntent();
        return receiveIntent.getStringExtra(
                GameListRecyclerAdapter.ViewHolder.STEAM_ID_KEY);
    }

    /**
     * Get App ID (Game ID for Steam) from the intent (The games's App ID for this activity).
     *
     * @return The App ID
     */
    protected String getAppId() {
        Intent receiveIntent = getIntent();
        return receiveIntent.getStringExtra(
                GameListRecyclerAdapter.ViewHolder.APP_ID_KEY);
    }

    /**
     * Initialise view when information is received from the Steam API. Removes the current
     * ProgressBar and show the game's achievement and stats for the user.
     */
    protected void initialiseView() {
        this.mProgressBar.setVisibility(View.GONE);
        this.mGameNameTextView.setText(this.mGameHandler.getGameName());
        this.mGameNameTextView.setVisibility(View.VISIBLE);
        this.mNavigationTabStrip.setVisibility(View.VISIBLE);
        this.mViewPager.setVisibility(View.VISIBLE);
        this.mGameHandler.sort(GameHandler.SORT_BY_GAME_STAT_NAME);
        this.mGameHandler.sort(GameHandler.SORT_BY_GAME_ACHIEVEMENT_NAME);
    }

    /**
     * The Observer to getting games from the API. Used to initialise the views when the API call
     * is done to get all the game's information.
     *
     * @return The Observer for getting games.
     */
    public DisposableObserver<String> getGameObserver() {
        return new DisposableObserver<String>() {

            @Override
            public void onNext(@NonNull String text) {
                Log.d(GameActivity.LOG_TAG, "onNext: getGameObserver " + text);
            }

            @Override
            public void onError(@NonNull Throwable e) {
                Log.e(GameActivity.LOG_TAG, "onError: getGameObserver", e);
            }

            /**
             * When the Observables has been finished, initialise the view for this activity.
             */
            @Override
            public void onComplete() {
                Log.d(GameActivity.LOG_TAG, "onComplete: getGameObserver");
                initialiseView();
            }
        };
    }

    /**
     * Inner class - Adapter for the ViewPager. It allows us to display multiple fragments in one
     * activity. This will show 2 fragments, GameAchievementFragment and GameStatFragment.
     */
    public class GamePagerAdapter extends FragmentPagerAdapter {
        private List<Fragment> mFragments;

        /**
         * Instantiates a new Game pager adapter.
         * @param fragmentManager The Fragment Manager
         */
        public GamePagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
            this.mFragments = new ArrayList<>();
            this.mFragments.add(GameAchievementFragment.newInstance());
            this.mFragments.add(GameStatFragment.newInstance());
        }

        /**
         * Get a fragment.
         * @param position Position of the fragment.
         * @return Fragment from this.mFragment
         */
        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        /**
         * Get number of Fragments in adapter.
         * @return Number of Fragments
         */
        @Override
        public int getCount() {
            return mFragments.size();
        }
    }
}
