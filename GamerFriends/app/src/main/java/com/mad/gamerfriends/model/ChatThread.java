package com.mad.gamerfriends.model;

import com.google.firebase.database.Exclude;

import java.util.ArrayList;
import java.util.List;

/**
 * A chat thread between 2 users.
 * @author Wirawan Tjo
 * @version 1.0
 */
public class ChatThread {
    private String mThreadId;
    private List<ChatMessage> mMessages;

    public ChatThread() {
        new ChatThread(null);
    }

    public ChatThread(String threadId) {
        this.mThreadId = threadId;
        this.mMessages = new ArrayList<>();
    }

    @Exclude
    public String getThreadId() {
        return this.mThreadId;
    }

    public void setThreadId(String threadId) {
        this.mThreadId = threadId;
    }

    public List<ChatMessage> getMessages() {
        return this.mMessages;
    }

    public void setMessages(List<ChatMessage> messages) {
        this.mMessages = messages;
    }
}
