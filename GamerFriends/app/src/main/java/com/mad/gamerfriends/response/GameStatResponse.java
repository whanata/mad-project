package com.mad.gamerfriends.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by whanata on 24/9/17.
 */

public class GameStatResponse {
    @SerializedName("name")
    private String mId;
    @SerializedName("displayName")
    private String mName;
    @SerializedName("defaultvalue")
    private String mDefaultValue;

    public GameStatResponse(String mId, String mDefaultValue, String mName) {
        this.mId = mId;
        this.mDefaultValue = mDefaultValue;
        this.mName = mName;
    }

    public String getId() {
        return this.mId.replaceAll("\\.|\\#|\\$|\\[|\\]", "_");
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getDefaultValue() {
        return this.mDefaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.mDefaultValue = defaultValue;
    }

    public String getName() {
        return this.mName;
    }

    public void setName(String name) {
        this.mName = name;
    }
}
