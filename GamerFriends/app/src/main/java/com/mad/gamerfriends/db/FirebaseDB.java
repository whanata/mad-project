package com.mad.gamerfriends.db;

import android.util.Log;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.mad.gamerfriends.exception.FirebaseDbException;
import com.mad.gamerfriends.model.ChatMessage;
import com.mad.gamerfriends.model.User;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;

/**
 * Wrapper Class for FireBase DB API. All Firebase DB calls will be using methods in this class and
 * therefore hide the implementation from the other classes. This class is also a singleton.
 * @author Wirawan Tjo
 * @version 1.0
 */
public class FirebaseDb {
    public static final String LOG_TAG = "FirebaseDb";

    public static final String USER_KEY = "user";
    public static final String FRIEND_LIST_KEY = "friend";
    public static final String GAME_KEY = "game";
    public static final String USER_GAME_KEY = "user_game";
    public static final String CHAT_KEY = "chat";
    public static final String CHAT_MEMBERS_KEY = "chat_members";
    public static final String CHAT_THREAD_KEY = "chat_thread";

    private static FirebaseDb sInstance = null;

    private FirebaseDatabase mFirebaseDatabase;

    /**
     * Gets Singleton Instance of FirebaseDB.
     *
     * @return Firebase DB Instance
     */
    public static synchronized FirebaseDb getInstance() {
        if (FirebaseDb.sInstance == null) {
            FirebaseDb.sInstance = new FirebaseDb();
        }

        return FirebaseDb.sInstance;
    }

    /**
     * Initiate new instance of FirebaseDatabase in the wrapper class FirebaseDB
     */
    private FirebaseDb() {
        this.mFirebaseDatabase = FirebaseDatabase.getInstance();
    }

    /**
     * Get a reference from FirebaseDatabase
     * @param path Path of reference
     * @return The path reference
     */
    private DatabaseReference getReference(String path) {
        return this.mFirebaseDatabase.getReference(path);
    }

    /**
     * Add user to Firebase Database.
     *
     * @param user A user
     */
    public void addUser(User user) {
        String path = String.format("/%s/", FirebaseDb.USER_KEY);
        DatabaseReference ref = this.getReference(path);
        ref.child(user.getSteamId()).setValue(user);
    }

    /**
     * Add friend to Firebase Database.
     *
     * @param currentUserId Steam ID
     * @param friendId      Friend's Steam ID
     */
    public void addFriend(String currentUserId, String friendId) {
        String path = String.format("/%s/", FirebaseDb.FRIEND_LIST_KEY);
        DatabaseReference ref = this.getReference(path);
        ref.child(currentUserId).child(friendId).setValue(true);
    }

    /**
     * Add chat thread to Firebase Database.
     *
     * @param currentUserId  Steam ID
     * @param receiverUserId Receiver's Steam ID
     * @return the string
     */
    public String addChatThread(String currentUserId, String receiverUserId) {
        String path;
        // Add Chat Members
        path = String.format("/%s/", FirebaseDb.CHAT_MEMBERS_KEY);
        DatabaseReference chatMemberRef = this.getReference(path).push();
        // Get thread ID
        String threadId = chatMemberRef.getKey();
        chatMemberRef.child(currentUserId).setValue(true);
        chatMemberRef.child(receiverUserId).setValue(true);

        // Add in both Chat (Both user ID)
        path = String.format("/%s/", FirebaseDb.CHAT_KEY);
        DatabaseReference chatRef = this.getReference(path);
        chatRef.child(currentUserId).push().setValue(threadId);
        chatRef.child(receiverUserId).push().setValue(threadId);

        return threadId;
    }

    /**
     * Add chat message to Firebase DB.
     *
     * @param threadId Thread ID
     * @param message  Single message
     */
    public void addChatMessage(String threadId, ChatMessage message) {
        DatabaseReference ref = this.getReference(FirebaseDb.CHAT_THREAD_KEY);
        ref.child(threadId).push().setValue(message);
    }

    /**
     * Gets chat query from Firebase DB (Used in Firebase DB - UI for chat)
     *
     * @param threadId Thread ID
     * @return Chat Query
     */
    public Query getChatQuery(String threadId) {
        String path = String.format("/%s/%s", FirebaseDb.CHAT_THREAD_KEY, threadId);
        return this.getReference(path);
    }

    /**
     * Create listener to listen on FirebaseDB calls of change in data
     * @param path The key/path of the value in FirebaseDB
     * @param emitter Emitter to track in the Observable when an action is done, have an error or
     *                completed
     * @return Listerner
     */
    private ValueEventListener getValueEventListener(
            final String path,
            final ObservableEmitter<DataSnapshot> emitter) {
        return new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                @SuppressWarnings("unchecked")
                Object result = dataSnapshot.getValue();
                if (result != null) {
                    emitter.onNext(dataSnapshot);
                } else {
                    emitter.onError(
                            new FirebaseDbException(path, FirebaseDbException.EMPTY_RESULT));
                }
                emitter.onComplete();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                emitter.onError(new FirebaseDbException(path, databaseError.toException()));
                emitter.onComplete();
            }
        };
    }

    /**
     * Get an Observable with the result of a DataSnapshot for a specific path in Firebase DB
     * @param path Key/Path of the value in FirebaseDB
     * @return Observable with DataSnapshot as the result
     */
    private Observable<DataSnapshot> getData(final String path) {
        Log.d(FirebaseDb.LOG_TAG, "getDbData: " + path);

        return Observable.create(new ObservableOnSubscribe<DataSnapshot>() {
            @Override
            public void subscribe(@NonNull final ObservableEmitter<DataSnapshot> emitter) {

                DatabaseReference ref = getReference(path);

                ValueEventListener listener = getValueEventListener(path, emitter);

                ref.addListenerForSingleValueEvent(listener);
            }
        });
    }

    /**
     * Get a user summary information
     *
     * @param steamId Steam ID
     * @return Observable for a DataSnapshot of user
     */
    public Observable<DataSnapshot> getUser(String steamId) {
        String path = String.format("/%s/%s", FirebaseDb.USER_KEY, steamId);
        return this.getData(path);
    }

    /**
     * Get a friend list.
     *
     * @param steamId Steam ID
     * @return Observable of a DataSnapshot of a list of friends
     */
    public Observable<DataSnapshot> getFriendList(String steamId) {
        String path = String.format("/%s/%s", FirebaseDb.FRIEND_LIST_KEY, steamId);
        return this.getData(path);
    }

    /**
     * Get all threads and their members where one of the member is the currentId supplied.
     *
     * @param currentId Steam ID
     * @return Observable of the ChatMembers
     */
    public Observable<DataSnapshot> getChatMembers(String currentId) {
        String path = String.format("/%s/%s", FirebaseDb.CHAT_KEY, currentId);
        return this.getData(path)
                .flatMapIterable(new Function<DataSnapshot, Iterable<String>>() {

                    @Override
                    public Iterable<String> apply(@NonNull DataSnapshot dataSnapshot)
                            throws Exception {
                        List<String> threadIds = new ArrayList<>();
                        @SuppressWarnings("unchecked")
                        Map<String, String> result = (Map<String, String>) dataSnapshot.getValue();
                        for (Map.Entry<String, String> row : result.entrySet()) {
                            Log.d(FirebaseDb.LOG_TAG, "Thread ID: " + row.getValue());
                            threadIds.add(row.getValue());
                        }
                        return threadIds;
                    }
                })
                .flatMap(new Function<String, ObservableSource<DataSnapshot>>() {

                    @Override
                    public ObservableSource<DataSnapshot> apply(@NonNull String threadId)
                            throws Exception {
                        String path =
                                String.format("/%s/%s", FirebaseDb.CHAT_MEMBERS_KEY, threadId);
                        return getData(path);
                    }
                });
    }
}
