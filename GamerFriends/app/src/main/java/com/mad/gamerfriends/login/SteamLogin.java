package com.mad.gamerfriends.login;

import android.app.Activity;
import android.net.Uri;

import com.mad.gamerfriends.helper.PreferenceHelper;

/**
 * Handles steam authentication
 * @author Wirawan Tjo
 * @version 1.0
 */
public class SteamLogin {
    public static final String LOG_TAG = "SteamLogin";
    public static final String REALM_PARAM = "com.mad.gamersfriends";

    public static final String STEAM_ID_KEY = "STEAM_ID_KEY";

    // Creating URL
    public static final String HTTP = "http";
    public static final String AUTHORITY = "steamcommunity.com";
    public static final String PATH_1 = "openid";
    public static final String PATH_2 = "login";
    public static final String CLAIMED_ID = "http://specs.openid.net/auth/2.0/identifier_select";
    public static final String IDENTITY = "http://specs.openid.net/auth/2.0/identifier_select";
    public static final String MODE = "checkid_setup";
    public static final String NS = "http://specs.openid.net/auth/2.0";
    public static final String REALM = "https://%s";
    public static final String RETURN_TO = "https://%s/signin/";

    protected Activity mActivity;

    /**
     * Constructor - Get activity
     * @param activity
     */
    public SteamLogin(Activity activity) {
        this.mActivity = activity;
    }

    /**
     * Build URL using the constants above. The URL is for Steam Authentication of a specific user.
     * @return URL of Steam Authentication for a specific user
     */
    public String getUrl() {
        Uri.Builder uriBuilder = new Uri.Builder();
        uriBuilder.scheme(SteamLogin.HTTP)
                .authority(SteamLogin.AUTHORITY)
                .appendPath(SteamLogin.PATH_1)
                .appendPath(SteamLogin.PATH_2)
                .appendQueryParameter("openid.claimed_id", SteamLogin.CLAIMED_ID)
                .appendQueryParameter("openid.identity", SteamLogin.IDENTITY)
                .appendQueryParameter("openid.mode", SteamLogin.MODE)
                .appendQueryParameter("openid.ns", SteamLogin.NS)
                .appendQueryParameter("openid.realm",
                        String.format(SteamLogin.REALM, SteamLogin.REALM_PARAM))
                .appendQueryParameter("openid.return_to",
                        String.format(SteamLogin.RETURN_TO, SteamLogin.REALM_PARAM));
        return uriBuilder.build().toString();
    }

    /**
     * Get Steam ID in Shared Preference
     * @return Steam ID
     */
    public String getUserId() {
        PreferenceHelper preference = new PreferenceHelper(this.mActivity);
        return preference.getString(SteamLogin.STEAM_ID_KEY);
    }

    /**
     * Get Steam ID from the URL provided by Steam Authentication.
     * @param uri Uri of the callback from Steam Authentication
     * @return Steam ID
     */
    public String getUserId(Uri uri) {
        Uri userAccountUrl = Uri.parse(uri.getQueryParameter("openid.identity"));
        String userId = userAccountUrl.getLastPathSegment();

        PreferenceHelper preference = new PreferenceHelper(this.mActivity);
        preference.addString(SteamLogin.STEAM_ID_KEY, userId);
        preference.commit();
        return userId;
    }
}

