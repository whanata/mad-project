package com.mad.gamerfriends.handler;

/**
 * Child class of UserHandler. This is a singleton class since there can be only one
 * UserHandler.
 * @author Wirawan Tjo
 * @version 1.0
 */
public class CurrentUserHandler extends UserHandler {
    private static CurrentUserHandler sInstance;

    /**
     * Get instance of CurrentUserHandler if exist, if not create new instance of this.
     * @return CurrentUserHandler instance
     */
    public static synchronized CurrentUserHandler getInstance() {
        if (sInstance == null) {
            sInstance = new CurrentUserHandler();
        }

        return sInstance;
    }

    /**
     * Constructor - create UserHandler
     */
    private CurrentUserHandler() {
        super("");
    }

    /**
     * Remove instance
     */
    public void logOut() {
        CurrentUserHandler.sInstance = null;
    }
}
