package com.mad.gamerfriends.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.mad.gamerfriends.R;
import com.mad.gamerfriends.handler.GameHandler;
import com.mad.gamerfriends.model.GameStat;

import java.text.DecimalFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Adapter Class for the GameStatFragment RecyclerView. Shows the Stat List for a
 * specific game by a specific user
 * @author  Wirawan Tjo
 * @version 1.0
 */
public class GameStatRecyclerAdapter extends RecyclerView.Adapter<GameStatRecyclerAdapter.ViewHolder> {
    public static final String LOG_TAG = "GameStatRecyclerAdapter";

    private GameHandler mGameHandler;

    /**
     * Instantiates GameStatRecyclerAdapter
     * @param gameHandler GameHandler used in the activity for this adapter.
     */
    public GameStatRecyclerAdapter(GameHandler gameHandler) {
        this.mGameHandler = gameHandler;
    }

    /**
     * Inner class - Represents each item in the RecyclerView. Each instance of this class is one
     * item in the RecyclerView.
     */
    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.item_game_stat_counter_TextView)
        protected TextView mStatCounterTextView;
        @BindView(R.id.item_game_stat_name_TextView)
        protected TextView mStatNameTextView;

        /**
         * The constructor for this class.
         * Gets all the views (widgets and layout) for this each row of the list of stats.
         * @param itemView The view of the item layout
         */
        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        /**
         * Sets the Stat Information to the item layout.
         * @param stat Stat for this item
         */
        public void setStat(GameStat stat) {
            String statName = "";
            if (!stat.getName().trim().equals("")) {
                statName = stat.getName();
            } else {
                statName = stat.getId();
            }
            statName = statName.replace("_", " ").toLowerCase();
            String statValue = new DecimalFormat("##.##").format(stat.getValue());
            this.mStatCounterTextView.setText(statValue);
            this.mStatNameTextView.setText(statName);
        }
    }

    /**
     * Create a single ViewHolder.
     * @param parent The parent view for the adapter
     * @param viewType Type of view
     * @return new ViewHolder Object
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_game_stat, parent, false);
        return new ViewHolder(itemView);
    }

    /**
     * Bind ViewHolder to a Stat Object
     * @param holder The ViewHolder Object.
     * @param position Position of the ViewHolder in context of the RecylerView
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        GameStat stat = this.getGameStats().get(position);
        holder.setStat(stat);
    }

    /**
     * Count of the rows in the RecyclerView.
     * @return Count of rows in the RecyclerView.
     */
    @Override
    public int getItemCount() {
        return this.getGameStats().size();
    }

    /**
     * Make mGameHandler = null to make sure it does not leak the activity to another class.
     * @param recyclerView
     */
    @Override
    public void onDetachedFromRecyclerView(RecyclerView recyclerView) {
        super.onDetachedFromRecyclerView(recyclerView);
        this.mGameHandler = null;
    }

    /**
     * Get GameStats used by this adapter.
     * @return GameStats used for this adapter.
     */
    private List<GameStat> getGameStats() {
        return this.mGameHandler.getStats();
    }
}

