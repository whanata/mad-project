package com.mad.gamerfriends.model;

import com.google.firebase.database.Exclude;
import com.mad.gamerfriends.response.GameAchievementResponse;
import com.mad.gamerfriends.response.UserAchievementResponse;

import java.util.Comparator;

/**
 * Game Achievement for a specific user on a specific game.
 * @author Wirawan Tjo
 * @version 1.0
 */
public class GameAchievement {
    /**
     * A comparator constant for ordering name ascending.
     * Make sure that if there isn't a name present user the ID.
     */
    public static Comparator<GameAchievement> COMPARE_BY_NAME_ASC = new Comparator<GameAchievement>() {
        @Override
        public int compare(GameAchievement achievement1, GameAchievement achievement) {
            String name1;
            String name2;

            if (!achievement1.getName().equals("")) {
                name1 = achievement1.getName();
            } else {
                name1 = achievement1.getId();
            }

            if (!achievement.getName().equals("")) {
                name2 = achievement.getName();
            } else {
                name2 = achievement.getId();
            }
            return name1.toLowerCase().compareTo(name2.toLowerCase());
        }
    };


    private String mId;
    private String mName;
    private String mDefaultValue;
    private Boolean mHidden;
    private String mDescription;
    private String mIconUrl;
    private String mIconGrayUrl;
    private Boolean mAchieved;

    public GameAchievement() {
        this.mAchieved = false;
    }

    /**
     * Copy information from GameAchievementResponse to the game.
     * @param response GameAchievementResponse
     */
    public GameAchievement(GameAchievementResponse response) {
        this();
        this.mId = response.getId();
        this.mName = response.getName();
        this.mDefaultValue = response.getDefaultValue();
        this.setRawHidden(response.getHidden());
        this.mDescription = response.getDescription();
        this.mIconUrl = response.getIcon();
        this.mIconGrayUrl = response.getIconGray();
    }

    /**
     * Check if the achievement was achieved, set this.mAchieved.
     * @param response UserAchievementResponse
     */
    public void addUserAchievementResponse(UserAchievementResponse response) {
        this.mAchieved = (response.getAchieved().equals("1"));
    }

    public String getId() {
        return this.mId;
    }

    public void setId(String id) {
        this.mId = id;
    }

    public String getName() {
        return this.mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getDefaultValue() {
        return this.mDefaultValue;
    }

    public void setDefaultValue(String defaultValue) {
        this.mDefaultValue = defaultValue;
    }

    public Boolean getHidden() {
        return this.mHidden;
    }

    public void setHidden(boolean hidden) {
        this.mHidden = hidden;
    }

    public void setRawHidden(int hidden) {
        // hidden = 0 (False) else True
        this.mHidden = !(hidden == 0);
    }

    public String getDescription() {
        return this.mDescription;
    }

    public void setDescription(String description) {
        this.mDescription = description;
    }

    public String getIconUrl() {
        return this.mIconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.mIconUrl = iconUrl;
    }

    public String getIconGrayUrl() {
        return this.mIconGrayUrl;
    }

    public void setIconGrayUrl(String iconGrayUrl) {
        this.mIconGrayUrl = iconGrayUrl;
    }

    @Exclude
    public Boolean getAchieved() {
        return this.mAchieved;
    }

    public void setAchieved(Boolean achieved) {
        this.mAchieved = achieved;
    }
}
