package com.mad.gamerfriends.response;

import com.google.gson.annotations.SerializedName;

/**
 * Created by whanata on 24/9/17.
 */

public class UserStatResponse {
    @SerializedName("name")
    private String mId;
    @SerializedName("value")
    private String mValue;

    public UserStatResponse(String mId, String mValue) {
        this.mId = mId;
        this.mValue = mValue;
    }

    public String getId() {
        return this.mId.replaceAll("\\.|\\#|\\$|\\[|\\]", "_");
    }

    public void setId(String name) {
        this.mId = name;
    }

    public String getValue() {
        return this.mValue;
    }

    public void setValue(String value) {
        this.mValue = value;
    }
}
