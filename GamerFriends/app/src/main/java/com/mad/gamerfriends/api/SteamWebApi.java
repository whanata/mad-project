package com.mad.gamerfriends.api;

import com.mad.gamerfriends.api_client.SteamWebClient;
import com.mad.gamerfriends.response.FriendListResponse;
import com.mad.gamerfriends.response.FriendResponse;
import com.mad.gamerfriends.response.GameSchemaResponse;
import com.mad.gamerfriends.response.OwnedGameListResponse;
import com.mad.gamerfriends.response.UserGameStatResponse;
import com.mad.gamerfriends.response.UserSummaryResponse;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Class for Steam Web API. All API Calls will be methods in this class, the retrofit2 methods
 * will not be shown in any other classes. This is basically calling the Steam Web API specifically.
 * This class is also a singleton.
 * @author  Wirawan Tjo
 * @version 1.0
 */
public class SteamWebApi {
    public static final String LOG_TAG = "SteamWebApi";
    public static final String BASE_URL = "http://api.steampowered.com/";
    public static final String API_KEY = "14F75CC8A70A2E8ED641F40C9472B558";
    public static final int USER_SUMMARY_LIMIT = 100;

    private static SteamWebApi sInstance = null;

    private SteamWebClient mClient;

    /**
     * Create new instance of SteamWebApi if there is no instance of it, else use the instance that
     * exist.
     * @return
     */
    public static synchronized SteamWebApi getInstance() {
        if (SteamWebApi.sInstance == null) {
            SteamWebApi.sInstance = new SteamWebApi();
        }

        return SteamWebApi.sInstance;
    }

    /**
     * Instantiates SteamWebApi
     */
    private SteamWebApi() {
        this.buildRetroFit(SteamWebApi.BASE_URL);
    }

    /**
     * Build retrofit object
     * @param url Base URL for the API call
     */
    private void buildRetroFit(String url) {
        OkHttpClient client = new OkHttpClient.Builder().build();
        Retrofit retrofit =
                new Retrofit.Builder()
                        .baseUrl(url)
                        .client(client)
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

        this.mClient = retrofit.create(SteamWebClient.class);
    }

    /**
     * Get User Summary call as an observable. API Call.
     * @param steamIds One or more steam IDs, seperated by comma
     * @return Observable of user summary information.
     */
    public Observable<UserSummaryResponse> getUserSummaries(
            String steamIds) {
        return this.mClient.getUserSummary(SteamWebApi.API_KEY, steamIds);
    }

    /**
     * Get Friend List call as an observable. API Call.
     * @param steamId Steam ID of user
     * @return Observable of friend list information
     */
    public Observable<UserSummaryResponse> getFriendList(String steamId) {
        return this.mClient.getFriendList(SteamWebApi.API_KEY, steamId)
                .flatMapIterable(new Function<FriendListResponse, Iterable<String>>() {

                    /**
                     * Split each response of User ID from friend list as separate calls to
                     * user summary api call
                     */
                    @Override
                    public Iterable<String> apply(@NonNull FriendListResponse response)
                            throws Exception {
                        return splitFriendIds(response);
                    }
                })
                .flatMap(new Function<String, ObservableSource<UserSummaryResponse>>() {

                    /**
                     * Get User Summary Response for the specific steamIds.
                     * @param steamIds List of steamIds separated by commas
                     * @return Observable of getUserSummary
                     * @throws Exception The exception
                     */
                    @Override
                    public ObservableSource<UserSummaryResponse> apply(@NonNull String steamIds)
                            throws Exception {
                        return mClient.getUserSummary(SteamWebApi.API_KEY, steamIds);
                    }
                });
    }

    /**
     * Get Owned Game List for user as an Observable. API Call.
     * @param steamId steam ID for user with Game List.
     * @return Observable of Owned Game List
     */
    public Observable<OwnedGameListResponse> getOwnedGames(String steamId) {
        return this.mClient.getOwnedGames(SteamWebApi.API_KEY, steamId);
    }

    /**
     * Get stats for a user in a specific game as an Observable. API Call.
     * @param steamId user's steam ID
     * @param appId game's app ID from Steam
     * @return Observable of stats for the user in a specific game
     */
    public Observable<UserGameStatResponse> getUserGameStats(String steamId, String appId) {
        return this.mClient.getUserGameStats(SteamWebApi.API_KEY, appId, steamId);
    }

    /**
     * Get information about a specific game as an Observable through an API call.
     * @param appId game's App ID from Steam
     * @return Observable of Game Information
     */
    public Observable<GameSchemaResponse> getGameSchema(String appId) {
        return this.mClient.getGameSchema(SteamWebApi.API_KEY, appId);
    }

    /**
     * Split the response to separate list of steam ID where each list will have up to
     * SteamWebApi.USER_SUMMARY_LIMIT
     * @param response FriendList Response Class
     * @return List of string where each item is a list of steam ID separated by a comma
     */
    private List<String> splitFriendIds(FriendListResponse response) {
        int counter = 0;
        List<String> friendIdList = new ArrayList<>();
        List<FriendResponse> friends = response.getFriendsList();
        String friendIds = "";

        if (friends != null) {
            for (FriendResponse friend : friends) {
                String friendId = friend.getSteamId();
                friendIds += friendId + ",";
                counter ++;

                if (counter % SteamWebApi.USER_SUMMARY_LIMIT == 0 || friends.size() == counter) {
                    friendIdList.add(friendIds.replace(",$", ""));
                    counter = 0;
                }
            }
        }

        return friendIdList;
    }
}
